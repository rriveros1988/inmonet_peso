<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');

	if(count($_POST) >= 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$numeroOperacion = $_POST['numeroOperacion'];
    		$row = consultaPagosEscrituraContable($codigoProyecto, $numeroOperacion);

      if(is_array($row))
      {

				$results = array(
						"sEcho" => 1,
						"iTotalRecords" => count($row),
						"iTotalDisplayRecords" => count($row),
						"aaData"=>$row
				);

				echo json_encode($results);
      }
      else{
		$results = array(
            "sEcho" => 1,
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData"=>[]
        );
        echo json_encode($results);
      }
	}
	else{
		echo "Sin datos";
	}
?>
