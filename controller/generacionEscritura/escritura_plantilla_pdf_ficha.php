<?php
  //ini_set('display_errors', 'On');
  // require('../../model/consultas.php');
  $comCorretaje = consultaComisionPromesaCorretaje($_SESSION['escrituraCodigoProyecto'], $_SESSION['escrituraNumeroOperacion'], $_SESSION['escrituraNumeroDepto'])[0];

  if($comCorretaje['IDACCION'] == '1'){
    $montoUF = $comCorretaje['VALORUF']*($comCorretaje['COMISIONLVCLIENTE']/100);
  }
  else{
    $montoUF = $comCorretaje['VALORUF']*($comCorretaje['COMISIONVENDUENO']/100);
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 35px;
      margin-right:35px;
      margin-top: 20px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 45px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 45px;
    }
    #cabecera3Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 400px;
    }
    #cabecera3Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 400px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 0;
      margin: 0;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 9px; font-family: Arial">

    <table id="tablaOC">
      <tr>
        <td id="cabecera1Izquierda">
          <img src="../../view/img/logos/living_logo.png" style='height: 60px;'>
        </td>
        <td id="cabecera1Derecha">
          <?php
            echo "<img src='" . $_SESSION['escrituraLogoProyecto'] . "' style='height: 60px;'>";
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: center; width:720px; font-size: 9px;">
          <b>ESCRITURA DE <?php if($_SESSION["escrituraAccion"] == "Venta"){
            echo "COMPRA";
          }
          else{
            echo "ARRIENDO";
          }
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: center; width:720px; font-size: 9px;">

        </td>
      </tr>
      <tr>

      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px;">

        </td>
        <td style="text-align: center; width: 100px;">

        </td>
        <td style="text-align: left; width: 280px;">

        </td>
        <td style="text-align: right; font-size: 9px; width: 100px; padding-bottom: 5px; padding-top: 5px;">

        </td>
        <td style="text-align: right; font-size: 9px; width: 100px; padding-bottom: 5px; padding-top: 5px;">
          <?php echo $_SESSION['escrituraFecha']; ?>
          <br/>
          Numero Operación: <b><?php echo $_SESSION['numeroOperacion']; ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <b>1) Individualización del Oferente Comprador.</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Nombre
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraNombreCliente']) . ' ' . strtoupper($_SESSION['escrituraApellidoCliente']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Cedula de Identidad
        </td>
        <td style="text-align: left; width: 122px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraRutCliente']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Nacionalidad
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraNacionalidadCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px; font-size: 9px;">
          Estado Civil
        </td>
        <td style="text-align: left; width: 100px; height: 10px; padding-left: 5px; font-size: 9px;">
          <?php
            echo strtoupper($_SESSION['escrituraEstadoCivilIDCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 490px; height: 10px;font-size:8px;">
          1. Soltero | 2. Separado | 3.Viudo | 4. Cas. Soc. Conyugal | 5. Cas. Sep. Bienes | 6. Cas. Part. Ganancias | 7. Divorciado
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Fecha Nacimiento
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            $fechaNac = new Datetime($_SESSION['escrituraFechaNacCliente1']);
            echo $fechaNac->format('d/m/Y');
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Profesión/Actividad
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraProfesionCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Domicilio
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraDomicilioCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Comuna
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraComunaCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Ciudad
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraCiudadCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Región
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraRegionCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="height: 10px;">
          &nbsp;
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;margin-top:20px;">
          Teléfonos
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraTelefonoCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Email
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['escrituraMailCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <?php
      if($_SESSION['escrituraCodigoProyecto'] == "COR"){
        echo '<table style="margin-top: 20px; margin-left: 40px;">
          <tr>
            <td style="text-align: left; width: 720px; font-size: 9px;">
              <b>2) Individualización del Oferente Vendedor.</b>
            </td>
          </tr>
        </table>';
        echo
          '<table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
            <tr>
              <td style="text-align: left; width: 120px; height: 10px;">
                Nombre
              </td>
              <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">';
                  echo strtoupper($_SESSION['datosUnidadVendedor'][0]['NOMBRESVENDEDOR']) . ' ' . strtoupper($_SESSION['datosUnidadVendedor'][0]['APELLIDOSVENDEDOR']);
        echo '</td>
            </tr>';
        echo
          '<tr>
              <td style="text-align: left; width: 120px; height: 10px;">
                Cedula de identidad
              </td>
              <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">';
                  echo strtoupper($_SESSION['datosUnidadVendedor'][0]['RUTVENDEDOR']);
        echo '</td>
            </tr>';
        echo
          '<tr>
              <td style="text-align: left; width: 120px; height: 10px;">
                Teléfonos
              </td>
              <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">';
                  echo strtoupper($_SESSION['datosUnidadVendedor'][0]['CELULARVENDEDOR']);
        echo '</td>
            </tr>';
        echo
          '<tr>
              <td style="text-align: left; width: 120px; height: 10px;">
                Email
              </td>
              <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">';
                  echo strtoupper($_SESSION['datosUnidadVendedor'][0]['EMAILVENDEDOR']);
        echo '</td>
            </tr>
          </table>';
      }
    ?>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <?php
            if($_SESSION['escrituraCodigoProyecto'] == "COR"){
              echo '<b>3) Individualización de las Unidades</b>';
            }
            else{
              echo '<b>2) Individualización de las Unidades</b>';
            }
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
        <?php
                if($_SESSION['escrituraCodigoProyecto'] != "COR"){
                  if($_SESSION['bodegasClienteEscritura'] != '' && $_SESSION['estacionamientosClienteEscritura'] != ''){
                    for($i = 0; $i < (count($_SESSION['bodegasClienteEscritura']) + count($_SESSION['estacionamientosClienteEscritura'])); $i++){
                      if ($i == 0 ) {
                        echo '<tr style="margin-left:10px;">
                      <td style="text-align: left; width: 130px; font-size: 9px;">
                        ' . strtoupper($_SESSION['tipoUnidad']) . ': <b>' .  $_SESSION['escrituraNumeroDepto'] .
                      '</b></td>'.
                      '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) . ': <b>' .
                      consultaUnidadEspecificaReserva($_SESSION['bodegasClienteEscritura'][$i])[0]['CODIGO'].
                      '</b></td>';
                      }else{
                        if ($i >= count($_SESSION['bodegasClienteEscritura'])) {
                          if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1-count($_SESSION['bodegasClienteEscritura'])).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteEscritura'][$i-count($_SESSION['bodegasClienteEscritura'])])[0]['CODIGO'].'</b></td>';
                           }else{
                            echo '<td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1-count($_SESSION['bodegasClienteEscritura'])).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteEscritura'][$i-count($_SESSION['bodegasClienteEscritura'])])[0]['CODIGO'].'</b></td>';
                            }
                          }else{
                            if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClienteEscritura'][$i])[0]['CODIGO'].'</b></td>';
                            }else{
                              echo '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClienteEscritura'][$i])[0]['CODIGO'].'</b></td>';
                            }
                          }
                      }

                      if($i == (count($_SESSION['bodegasClienteEscritura']) + count($_SESSION['estacionamientosClienteEscritura'])-1)){
                        echo '</tr>';
                      }
                    }
                  }
                  else if($_SESSION['bodegasClienteEscritura'] == ''){
                    for($i = 0; $i < count($_SESSION['estacionamientosClienteEscritura']); $i++){
                      if ($i == 0 ) {
                        echo '<tr style="margin-left:10px;">
                      <td style="text-align: left; width: 130px; font-size: 9px;">
                        ' . strtoupper($_SESSION['tipoUnidad']) . ': <b>' .  $_SESSION['escrituraNumeroDepto'] .
                      '</b></td>'.
                      '<td style="text-align: left; width: 100px; font-size: 9px;">ESTAC' . ($i+1) . ': <b>' .
                      consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteEscritura'][$i])[0]['CODIGO'].
                      '</b></td>';
                      }else{
                          if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteEscritura'][$i])[0]['CODIGO'].'</b></td>';
                           }else{
                            echo '<td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteEscritura'][$i])[0]['CODIGO'].'</b></td>';
                            }
                      }

                      if($i == (count($_SESSION['estacionamientosClienteEscritura'])-1)){
                        echo '</tr>';
                      }
                    }
                  }
                  else if($_SESSION['estacionamientosClienteEscritura'] == ''){
                    for($i = 0; $i < count($_SESSION['bodegasClienteEscritura']); $i++){
                      if ($i == 0 ) {
                        echo '<tr style="margin-left:10px;">
                      <td style="text-align: left; width: 130px; font-size: 9px;">
                        ' . strtoupper($_SESSION['tipoUnidad']) . ': <b>' .  $_SESSION['escrituraNumeroDepto'] .
                      '</b></td>'.
                      '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) . ': <b>' .
                      consultaUnidadEspecificaReserva($_SESSION['bodegasClienteEscritura'][$i])[0]['CODIGO'].
                      '</b></td>';
                      }else{
                            if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClienteEscritura'][$i])[0]['CODIGO'].'</b></td>';
                            }else{
                              echo '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClienteEscritura'][$i])[0]['CODIGO'].'</b></td>';
                            }
                      }
                      if($i == (count($_SESSION['bodegasClienteEscritura'])-1)){
                        echo '</tr>';
                      }
                    }
                  }
                }
                if($_SESSION['escrituraCodigoProyecto'] == "COR"){
                  $datosUnidadCor = datosUnidadCorretaje($_SESSION['escrituraCodigoProyecto'], $_SESSION['escrituraNumeroDepto'], $_SESSION['escrituraAccion']);
                  echo '<tr style="margin-left:10px;">
                  <td style="text-align: left; width: 600px; font-size: 9px;">
                  ' . $_SESSION['tipoUnidad'] . ': <b>' .  $_SESSION['escrituraNumeroDepto'] . '</b> - Dirección: ' . $datosUnidadCor['DIRECCION'] .
                  '</td></tr>';
                }
        ?>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <b>Precio Total de la Compraventa UF: <?php echo str_replace('.',',',$_SESSION['escrituraTotalUF']); ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <?php
            if($_SESSION['escrituraCodigoProyecto'] == "COR"){
              echo '<b>4) Forma de pago</b>';
            }
            else{
              echo '<b>3) Forma de pago</b>';
            }
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 710px; line-height: 11px;">
          <?php echo "<b>a)</b> Con la suma de&nbsp;&nbsp;&nbsp;<b>UF " . str_replace('.',',',$_SESSION['escrituraReservaUF']) . "</b>&nbsp;&nbsp;&nbsp;equivalentes a la fecha <b>" . $_SESSION['escrituraFechaPagoReserva'] . "</b> por&nbsp;&nbsp;&nbsp;<b>$ " . number_format($_SESSION['escrituraValorPagoReserva'], 0, '.', '.') . "</b>&nbsp;&nbsp;&nbsp;que se pagaron como <b>Reserva</b> mediante&nbsp;<b>" . $_SESSION['escrituraFormaPagoNombreReserva'] . "</b>, del Banco/Emisor&nbsp;<b>" . $_SESSION['escrituraBancoReserva'] . "</b>, Nro de Serie&nbsp;<b>" . $_SESSION['escrituraSerieNroReserva'] . "</b>, Nro. Cheque&nbsp;<b>" . $_SESSION['escrituraNroTransChequeReserva'] . "</b>."; ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 710px; line-height: 11px;">
          <?php echo "<b>b)</b> Con la suma de&nbsp;&nbsp;&nbsp;<b>UF " . str_replace('.',',',$_SESSION['escrituraPiePromesaUF']) . "</b>&nbsp;&nbsp;&nbsp;equivalentes a la fecha <b>" . $_SESSION['escrituraFechaPagoPromesa'] . "</b> por&nbsp;&nbsp;&nbsp;<b>$ " . number_format($_SESSION['valorPiePromesa'], 0, '.', '.') . "</b>&nbsp;&nbsp;&nbsp;";

            if(number_format($_SESSION['valorPiePromesa'], 0, '.', '.') != '0'){
              echo "que se pagaron como <b>Pie Promesa</b> mediante ";
            }
            else{
              echo "que se pagaron como <b>Pie Promesa</b>.";
            }

            if(number_format($_SESSION['valorPiePromesa'], 0, '.', '.') != '0'){
              echo "<b>" . $_SESSION['escrituraFormaPagoNombrePromesa'] . "</b>, del Banco/Emisor&nbsp;<b>" .  $_SESSION['escrituraBancoPromesa'] . "</b>, Nro de Serie&nbsp;<b>" . $_SESSION['escrituraSerieNroPromesa'] . "</b>, Nro. Cheque&nbsp;<b>" . $_SESSION['escrituraNroTransChequePromesa'] . "</b>.";
            }
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 710px; line-height: 11px;">
          <?php echo "<b>c)</b> Con la suma de&nbsp;&nbsp;&nbsp;<b>UF " . str_replace('.',',',$_SESSION['escrituraPieCuotas']) . "</b>&nbsp;&nbsp;&nbsp;equivalentes a&nbsp;&nbsp;&nbsp;<b>$ " . number_format($_SESSION['valorPieSaldo'], 0, '.', '.') . "</b>&nbsp;&nbsp;&nbsp;";
            if(array_key_exists('escrituraCuotas', $_SESSION) && count($_SESSION['escrituraCuotas']) > 0){
              echo "que se pagaron en <b>" . $_SESSION['escrituraCuotasPie'] . " cuota</b>, como <b>Pie Cuotas</b> mediante:";
            }
            else{
              echo "que se pagaron como <b>Pie Cuotas</b>.";
            }
          ?>
        </td>
      </tr>
    </table>

      <?php
        if(array_key_exists('escrituraCuotas', $_SESSION) && count($_SESSION['escrituraCuotas']) > 0){
          for($c = 0; $c < count($_SESSION['escrituraCuotas']); $c++){
            echo '<table style="margin-top: 10px; margin-left: 60px; font-size: 9px;"><tr>';
            echo '<td style="width: 690px;"><b>'.$_SESSION['escrituraCuotas'][$c]['NUMEROCUOTA'].') '.consultaFormaPagoReservaEspecifica($_SESSION['escrituraCuotas'][$c]['FORMAPAGOCUOTA'])[0]['NOMBRE'].'</b>, del Banco/Emisor&nbsp;<b>'.$_SESSION['escrituraCuotas'][$c]['BANCOCUOTA'].'</b>, Nro de Serie&nbsp;<b>'.$_SESSION['escrituraCuotas'][$c]['SERIECHEQUECUOTA'].'</b>, Nro. Cheque&nbsp;<b>'.$_SESSION['escrituraCuotas'][$c]['NROCHEQUECUOTA'].'</b> por la suma de <b>$ '.number_format($_SESSION['escrituraCuotas'][$c]['VALORMONTOCUOTA'], 0, '.', '.').'</b>, correspondiente a <b>UF '.str_replace('.',',',$_SESSION['escrituraCuotas'][$c]['VALORUFCUOTA']).'</b> con Fecha de Pago&nbsp;';
            $fechaCuotaC = new Datetime($_SESSION['escrituraCuotas'][$c]['FECHAPAGOCUOTA']);
            echo '<b>'. $fechaCuotaC->format('d-m-Y') .'</b></td>';
            echo "</tr></table>";
          }
        }
      ?>
          <?php
            if(number_format($_SESSION['residualMonto'], 0, '.', '.') != '0'){
              echo '<table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
            <tr>
                <td style="text-align:left; width: 710px; line-height: 11px;">';
              echo "<b>d)</b> Con la suma de&nbsp;&nbsp;&nbsp;<b>$ " . number_format($_SESSION['residualMonto'], 0, '.', '.') . "</b>&nbsp;&nbsp;&nbsp;";
              if(number_format($_SESSION['residualMonto'], 0, '.', '.') != '0'){
                echo "a la fecha <b>" . $_SESSION['residualFechaCheque'] . "</b> que se pagaron como <b>Valor Diferencial UF</b> respecto a la UF del día <b>" . $_SESSION['escrituraFecha'] . "</b> mediante ";
              }
              else{
                echo "que se pagaron como <b>Valor Diferencial UF</b>.";
              }

              if(number_format($_SESSION['residualMonto'], 0, '.', '.') != '0'){
                echo "<b>" . $_SESSION['residualFormaPago'][0]['NOMBRE'] . "</b>, del Banco/Emisor&nbsp;<b>" .  $_SESSION['residualBanco'] . "</b>, Nro de Serie&nbsp;<b>" . $_SESSION['residualSerie'] . "</b>, Nro. Cheque&nbsp;<b>" . $_SESSION['residualNro'] . "</b>.";
              }
              echo '  </td>

              </tr>
            </table>';
            }
          ?>
    <table style="margin-top:10px; margin-left:40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 710px; line-height: 11px;">
          <?php
            if(number_format($_SESSION['residualMonto'], 0, '.', '.') == '0'){
              echo "<b>d)</b> Con la suma de&nbsp;&nbsp;&nbsp;<b>UF " . str_replace('.',',',$_SESSION['escrituraSaldoTotalUF']) . "</b>&nbsp;&nbsp;&nbsp;equivalentes a la fecha <b>" . $_SESSION['escrituraFechaPagoEscritura'] . "</b> por&nbsp;&nbsp;&nbsp;<b>$ " . number_format($_SESSION['escrituraValorPagoEscritura'], 0, '.', '.') . "</b>&nbsp;&nbsp;&nbsp;que se pagaron como <b>Saldo</b> mediante&nbsp;<b>" . $_SESSION['escrituraFormaPagoEscritura'][0]['NOMBRE'] . "</b>, del Banco/Emisor&nbsp;<b>" .  $_SESSION['escrituraBancoEscritura'] . "</b>, Nro de Serie&nbsp;<b>" . $_SESSION['escrituraSerieChequeEscritura'] . "</b>, Nro. Cheque&nbsp;<b>" . $_SESSION['escrituraNroChequeEscritura'] . "</b>.";
            }
            else{
              echo "<b>e)</b> Con la suma de&nbsp;&nbsp;&nbsp;<b>UF " . str_replace('.',',',$_SESSION['escrituraSaldoTotalUF']) . "</b>&nbsp;&nbsp;&nbsp;equivalentes a la fecha <b>" . $_SESSION['escrituraFechaPagoEscritura'] . "</b> por&nbsp;&nbsp;&nbsp;<b>$ " . number_format($_SESSION['escrituraValorPagoEscritura'], 0, '.', '.') . "</b>&nbsp;&nbsp;&nbsp;que se pagaron como <b>Saldo</b> mediante&nbsp;<b>" . $_SESSION['escrituraFormaPagoEscritura'][0]['NOMBRE'] . "</b>, del Banco/Emisor&nbsp;<b>" .  $_SESSION['escrituraBancoEscritura'] . "</b>, Nro de Serie&nbsp;<b>" . $_SESSION['escrituraSerieChequeEscritura'] . "</b>, Nro. Cheque&nbsp;<b>" . $_SESSION['escrituraNroChequeEscritura'] . "</b>.";
            }
          ?>
        </td>
      </tr>
    </table>
    <?php
      if($_SESSION['escrituraCodigoProyecto'] == "COR"){
        echo '<table style="margin-top: 20px; margin-left: 40px;">
          <tr>
            <td style="text-align: left; width: 720px; font-size: 9px;">
              <b>5) Comisiones</b>
            </td>
          </tr>
        </table>';
      }
    ?>
    <?php
      if($_SESSION['escrituraCodigoProyecto'] == "COR"){
        if($_SESSION['formaPagoCliente'][0]['MONTOPAGOCOM'] != '0'){
          $fechaC = new Datetime($_SESSION['formaPagoCliente'][0]['FECHAPAGOCOMISION']);
          echo
          '<table style="margin-left:40px; font-size: 9px;">'.
            '<tr>'.
              '<td>'.
              '&nbsp;'.
              '</td>'.
            '</tr>'.
            '<tr>'.
              '<td style="text-align: left; width: 720px;">'.
                // '<b>e)</b> Con la suma de UF ' . $_SESSION['MONTOUFCOMISION'] . ' equivalentes a esta fecha ' . $_SESSION['MONTOPESOCOMISION'] . ' que el Prominente Cliente declara pagar en este acto mediante ' . $_SESSION['FORMAPAGOCOMISION'] . ', del Banco/Emisor ' . $_SESSION['BANCOCOMISION'] . ', Nro de Serie ' . $_SESSION['SERIECOMISION'] . ', Nro. Cheque ' . $_SESSION['NROCOMISION'] . ' con Fecha de Pago ' . $_SESSION['FECHAPAGOCOMISION'] .'.'.
                '<b>a)</b> Con la suma de <b>UF ' . number_format($_SESSION['formaPagoCliente'][0]['VALORUF'], 2, ',', '.') . '</b> equivalentes a <b>$ ' . number_format($_SESSION['formaPagoCliente'][0]['MONTOPAGOCOM'], 0, '.', '.') . '</b> correspondientes a pago de comision que el Prominente Comprador documenta en esta acto mediante <b>' . $_SESSION['formaPagoCliente'][0]['FORMAPAGO'] . '</b> del Banco/Emisor <b>' . $_SESSION['formaPagoCliente'][0]['BANCOPROMESA'] .  '</b> Nro. serie <b>' . $_SESSION['formaPagoCliente'][0]['SERIECHEQUEPROMESA'] . '</b> Nro. cheque <b>' . $_SESSION['formaPagoCliente'][0]['NROCHEQUEPROMESA'] . '</b> con fecha <b>' . $fechaC->format('d-m-Y')  . '</b>.' .
              '</td>'.
            '</tr>'.
          '</table>';
          $fechaV = new Datetime($_SESSION['formaPagoCliente'][1]['FECHAPAGOCOMISION']);
          echo
          '<table style="margin-left:40px; font-size: 9px;">'.
            '<tr>'.
              '<td>'.
              '&nbsp;'.
              '</td>'.
            '</tr>'.
            '<tr>'.
              '<td style="text-align: left; width: 720px;">'.
                // '<b>e)</b> Con la suma de UF ' . $_SESSION['MONTOUFCOMISION'] . ' equivalentes a esta fecha ' . $_SESSION['MONTOPESOCOMISION'] . ' que el Prominente Cliente declara pagar en este acto mediante ' . $_SESSION['FORMAPAGOCOMISION'] . ', del Banco/Emisor ' . $_SESSION['BANCOCOMISION'] . ', Nro de Serie ' . $_SESSION['SERIECOMISION'] . ', Nro. Cheque ' . $_SESSION['NROCOMISION'] . ' con Fecha de Pago ' . $_SESSION['FECHAPAGOCOMISION'] .'.'.
                '<b>b)</b> Con la suma de <b>UF ' . number_format($_SESSION['formaPagoCliente'][1]['VALORUF'], 2, ',', '.') . '</b> equivalentes a <b>$ ' . number_format($_SESSION['formaPagoCliente'][1]['MONTOPAGOCOM'], 0, '.', '.') . '</b> correspondientes a pago de comision que el Prominente Vendedor documenta en esta acto mediante <b>' . $_SESSION['formaPagoCliente'][1]['FORMAPAGO'] . '</b> del Banco/Emisor <b>' . $_SESSION['formaPagoCliente'][1]['BANCOPROMESA'] .  '</b> Nro. serie <b>' . $_SESSION['formaPagoCliente'][1]['SERIECHEQUEPROMESA'] . '</b> Nro. cheque <b>' . $_SESSION['formaPagoCliente'][1]['NROCHEQUEPROMESA'] . '</b> con fecha <b>' . $fechaV->format('d-m-Y')  . '</b>.' .
              '</td>'.
            '</tr>'.
          '</table>';
        }
      }
    ?>
    <table style="margin-top: 50px; margin-left: 120px;">
      <tr>
        <td style="width: 150px; text-align: center;">

        </td>
        <td style="width: 240px;">

        </td>
        <td style="width: 150px; text-align: center;">
          <hr style="height: 1px;"/>
            P/Vendedor<br>
            <?php
            $user = consultaUsuarioId($_SESSION['idUsuario']);
            echo $user[0]['NOMBRE'] . '<br/>' . $user[0]['RUT'];
            ?>
        </td>
      </tr>
    </table>
  </body>
</html>
