<?php
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();

function pago($tasa, $monto, $meses){
  $I = $tasa / 12 / 100 ;
  $I2 = $I + 1 ;
  $I2 = pow($I2,-$meses) ;

  $CuotaMensual = ($I * $monto) / (1 - $I2);

  return $CuotaMensual;
}

$row = chequeaCliente($_POST['rutClienteCotizacion']);

if(!is_array($row)){
  if($_POST['rutClienteCotizacion'] != ''){
    $ingresaC = ingresaClienteCotiza($_POST['rutClienteCotizacion'],$_POST['nombreClienteCotizacion'],$_POST['apellidoClienteCotizacion'],$_POST['mailClienteCotizacion'],$_POST['celularClienteCotizacion'],'1',date('Y-m-d'));
  }
}
else{
  $actualizaC = actualizaClienteCotiza($_POST['rutClienteCotizacion'],$_POST['nombreClienteCotizacion'],$_POST['apellidoClienteCotizacion'],$_POST['mailClienteCotizacion'],$_POST['celularClienteCotizacion']);
}

$_SESSION['accionClienteCotizacion'] = $_POST['accionClienteCotizacion'];
$_SESSION['codProyectoClienteCotizacion'] = $_POST['codProyectoClienteCotizacion'];
$_SESSION['departamentoClienteCotizacion'] = $_POST['departaentoClienteCotizacion'];
$_SESSION['bodegasClienteCotizacion'] = $_POST['bodegasClienteCotizacion'];
$_SESSION['estacionamientosClienteCotizacion'] = $_POST['estacionamientosClienteCotizacion'];
$_SESSION['visitaClienteCotizacion'] = $_POST['visitaClienteCotizacion'];
$_SESSION['publicidadClienteCotizacion'] = $_POST['publicidadClienteCotizacion'];
$_SESSION['nombreProyectoClienteCotizacion'] = $_POST['nombreProyectoClienteCotizacion'];

$datosProyectoEsp = consultaDatosProyecto($_SESSION['codProyectoClienteCotizacion']);
$diasCot = $datosProyectoEsp[0]['DIASCOT'];

$_SESSION['diasCotProyectoCotizacion'] = $diasCot;


if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
  $_SESSION['nombreClienteCotizacion'] = $_POST['nombreClienteCotizacion'];
  $_SESSION['apellidoClienteCotizacion'] = $_POST['apellidoClienteCotizacion'];
  $_SESSION['rutClienteCotizacion'] = $_POST['rutClienteCotizacion'];
  $_SESSION['celularClienteCotizacion'] = $_POST['celularClienteCotizacion'];
  $_SESSION['mailClienteCotizacion'] = $_POST['mailClienteCotizacion'];
  $_SESSION['fechaCotizacion'] = date('d-m-Y');

  $domicilioCliente = domicilioCliente($_SESSION['rutClienteCotizacion']);

  $_SESSION['direccionCliClienteCotizacion'] = $domicilioCliente['DOMICILIO'] . ', ' . $domicilioCliente['COMUNA'];

  $datosUnidad = datosUnidadCorretaje($_SESSION['codProyectoClienteCotizacion'],$_SESSION['departamentoClienteCotizacion'],$_SESSION['accionClienteCotizacion']);
  $unidad2 = consultaUnidadEspecifica($datosUnidad['IDUNIDAD']);
  $_SESSION['tipoUnidad'] = $unidad2[0]['TIPOUNIDAD'];
  $_SESSION['direccionCorClienteCotizacion'] = $datosUnidad['DIRECCION'] . ', ' . $datosUnidad['COMUNA'];
  $_SESSION['precioCorClienteCotizacion'] = $datosUnidad['VALORUF'];
  $_SESSION['descripcion1CorClienteCotizacion'] = $datosUnidad['DESCRIPCION1'];
  $_SESSION['descripcion2CorClienteCotizacion'] = $datosUnidad['DESCRIPCION2'];
  if($_POST['accionClienteCotizacion'] === 'Venta'){
    $_SESSION['comisionCorClienteCotizacion']= $datosUnidad['COMISIONLVCLIENTE'];
  }
  else{
     $_SESSION['comisionCorClienteCotizacion'] = $datosUnidad['COMISIONVENCLIENTE'];
  }
  $temp = $_SESSION['descripcion1CorClienteCotizacion'];

  $desc1Array = explode("|", $temp);

  $_SESSION['descripcion1CorClienteCotizacion'] = '';

  for($i = 0; $i < count($desc1Array); $i++){
    if($i == 0){
      $_SESSION['descripcion1CorClienteCotizacion'] = $_SESSION['descripcion1CorClienteCotizacion'] . $desc1Array[$i];
    }
    else{
      $_SESSION['descripcion1CorClienteCotizacion'] = $_SESSION['descripcion1CorClienteCotizacion'] . ' ' . $desc1Array[$i];
    }
  }

  $_SESSION['totalFUFClienteCotizacion'] = $_SESSION['precioCorClienteCotizacion'];
  $_SESSION['departamentoUfClienteCotizacion'] = $_SESSION['precioCorClienteCotizacion'];
  $_SESSION['totalFUFClienteCotizacion'] = number_format($_SESSION['totalFUFClienteCotizacion'],2,'.','');


  $_SESSION['imgPlanoClienteCotizacion'] = '../' . $_POST['imgPlanoClienteCotizacion'];
  if($_SESSION['imgPlanoClienteCotizacion'] == '../' || $_POST['imgPlanoClienteCotizacion'] == '../view/img/imagenes_cliente/proyectos/PAT/unidad/plano/' || strlen($_POST['imgPlanoClienteCotizacion']) < 65){
    $_SESSION['imgPlanoClienteCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
  }
  if($_POST['accionClienteCotizacion'] == "Venta"){
    //Formas de pago - Datos desde post
    $_SESSION['reservaClienteCotizacion'] = $_POST['reservaClienteCotizacion']; //UF

    //Calculo de pie promesa
    if($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100 > $_SESSION['reservaClienteCotizacion']){
      $_SESSION['piePromesaClienteCotizacion'] = ($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100)-$_POST['reservaClienteCotizacion'];

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['saldoClienteCotizacion'] = $_POST['saldoClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }
    else{
      $_SESSION['piePromesaClienteCotizacion'] = 0;

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      if(($_SESSION['pieCuotasClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'] + ($_SESSION['reservaClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'])) > 100){
        $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - ($_SESSION['reservaClienteCotizacion']);
      }
      $_SESSION['saldoClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['pieCuotasClienteCotizacion'] -  $_SESSION['reservaClienteCotizacion'];//Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }

    $_SESSION['piePromesaClienteCotizacion'] = number_format($_SESSION['piePromesaClienteCotizacion'], 2, '.', '');
    $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['saldoClienteCotizacion'] - $_SESSION['piePromesaClienteCotizacion'] - $_SESSION['reservaClienteCotizacion'];
    $_SESSION['pieCuotasClienteCotizacion'] = number_format($_SESSION['pieCuotasClienteCotizacion'], 2, '.', '');
  }
  else{

  }
}
else{
  if($_POST['accionClienteCotizacion'] == "Venta"){
    //Datos desde query
    $_SESSION['logoProyectoCotizacion'] = '../' . $_POST['logoProyectoCotizacion'];
    if($_SESSION['logoProyectoCotizacion'] == '../'){
      $_SESSION['logoProyectoCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
    }
    $_SESSION['direccionProyectoCotizacion'] = $_POST['direccionProyectoCotizacion'];
    $_SESSION['fechaCotizacion'] = date('d-m-Y');

    //Desde post
    $_SESSION['nombreClienteCotizacion'] = $_POST['nombreClienteCotizacion'];
    $_SESSION['apellidoClienteCotizacion'] = $_POST['apellidoClienteCotizacion'];
    $_SESSION['rutClienteCotizacion'] = $_POST['rutClienteCotizacion'];
    $_SESSION['celularClienteCotizacion'] = $_POST['celularClienteCotizacion'];
    $_SESSION['mailClienteCotizacion'] = $_POST['mailClienteCotizacion'];
    $_SESSION['departaentoClienteCotizacion'] = $_POST['departaentoClienteCotizacion'];
    $_SESSION['fono1ClienteCotizacion'] = $_POST['fono1ClienteCotizacion'];
    $_SESSION['emailInmoClienteCotizacion'] = $_POST['emailInmoClienteCotizacion'];
    $_SESSION['webInmoClienteCotizacion'] = $_POST['webInmoClienteCotizacion'];

    //Query desde unidad
    $_SESSION['tipologiaClienteCotizacion'] = $_POST['tipologiaClienteCotizacion'];
    $_SESSION['modeloClienteCotizacion'] = $_POST['modeloClienteCotizacion'];
    $_SESSION['orientacionClienteCotizacion'] = $_POST['orientacionClienteCotizacion'];
    $_SESSION['m2UtilesClienteCotizacion'] = $_POST['m2UtilesClienteCotizacion'];
    $_SESSION['m2TerrazaClienteCotizacion'] = $_POST['m2TerrazaClienteCotizacion'];
    $_SESSION['m2TotalClienteCotizacion'] = $_POST['m2TotalClienteCotizacion'];

    //Desde post array
    if($_POST['estacionamientosClienteCotizacion'] == null){
      $_SESSION['estacionamientosClienteCotizacion'] = $_POST['cantEstacionamientos'];
    }
    else{
      $_SESSION['estacionamientosClienteCotizacion'] = $_POST['cantEstacionamientos'] + count($_POST['estacionamientosClienteCotizacion']);
    }
    if($_POST['bodegasClienteCotizacion'] == null){
      $_SESSION['bodegasClienteCotizacion'] = $_POST['cantBodegas'];
    }
    else{
      $_SESSION['bodegasClienteCotizacion'] = $_POST['cantBodegas'] + count($_POST['bodegasClienteCotizacion']);
    }

    $ch = curl_init();

		$fecha = new Datetime();
    $dia = $fecha->format('d');
		$mes = $fecha->format('m');
		$ano = $fecha->format('Y');

		$fecha = $fecha->format('d-m-Y');

	 // set url
		 $valor = $_POST['valorUFJqueryHoy'];

    $_SESSION['ufClienteCotizacion'] = $valor;

    //Datos desde query
    $_SESSION['departamentoUfClienteCotizacion'] = $_POST['departamentoUfClienteCotizacion'];
    $_SESSION['bodegaUfClienteCotizacion'] = 0;
    $_SESSION['estacionamientoUfClienteCotizacion'] = 0;

    //Calculo de valor final de bodegas
    for($i = 0; $i < count($_POST['bodegasClienteCotizacion']); $i++){
      $row = valorUnidad($_POST['bodegasClienteCotizacion'][$i]);
      $_SESSION['bodegaUfClienteCotizacion'] = $_SESSION['bodegaUfClienteCotizacion'] + $row['VALORUF'];
    }

    for($j = 0; $j < count($_POST['estacionamientosClienteCotizacion']); $j++){
      $row2 = valorUnidad($_POST['estacionamientosClienteCotizacion'][$j]);
      $_SESSION['estacionamientoUfClienteCotizacion'] = $_SESSION['estacionamientoUfClienteCotizacion'] + $row2['VALORUF'];
    }

    //Datos desde post
    $_SESSION['descuento1ClienteCotizacion'] = $_POST['descuento1ClienteCotizacion'];
    $_SESSION['descuento2ClienteCotizacion'] = $_POST['descuento2ClienteCotizacion'];

    //Calculos (simil venta de momento)
    $_SESSION['total2UFClienteCotizacion'] = $_SESSION['departamentoUfClienteCotizacion'] - ($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['descuento1ClienteCotizacion']/100);

    $_SESSION['totalFUFClienteCotizacion'] = ($_SESSION['departamentoUfClienteCotizacion']+$_SESSION['bodegaUfClienteCotizacion']+$_SESSION['estacionamientoUfClienteCotizacion']) -
    ($_SESSION['total2UFClienteCotizacion']*$_SESSION['descuento2ClienteCotizacion']/100) -
    ($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['descuento1ClienteCotizacion']/100);
    $_SESSION['bonoVentaCrearCotizacion'] = $_POST['bonoVentaCrearCotizacion'];
    if($_SESSION['departamentoUfClienteCotizacion'] != 0){
        $_SESSION['totalFUFClienteCotizacion']  = $_SESSION['totalFUFClienteCotizacion'] - $_POST['bonoVentaCrearCotizacion'];
    }

    $_SESSION['totalFUFClienteCotizacion'] = number_format($_SESSION['totalFUFClienteCotizacion'],2,'.','');

    //Dato desde query
    $_SESSION['imgPlanoClienteCotizacion'] = '../' . $_POST['imgPlanoClienteCotizacion'];
    if($_SESSION['imgPlanoClienteCotizacion'] == '../' || $_POST['imgPlanoClienteCotizacion'] == '../view/img/imagenes_cliente/proyectos/PAT/unidad/plano/' || strlen($_POST['imgPlanoClienteCotizacion']) < 65){
      $_SESSION['imgPlanoClienteCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
    }

    //Formas de pago - Datos desde post
    $_SESSION['reservaClienteCotizacion'] = $_POST['reservaClienteCotizacion']; //UF

    //Calculo de pie promesa
    if($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100 > $_SESSION['reservaClienteCotizacion']){
      $_SESSION['piePromesaClienteCotizacion'] = ($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100)-$_POST['reservaClienteCotizacion'];

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['saldoClienteCotizacion'] = $_POST['saldoClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }
    else{
      $_SESSION['piePromesaClienteCotizacion'] = 0;

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      if(($_SESSION['pieCuotasClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'] + ($_SESSION['reservaClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'])) > 100){
        $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - ($_SESSION['reservaClienteCotizacion']);
      }
      $_SESSION['saldoClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['pieCuotasClienteCotizacion'] -  $_SESSION['reservaClienteCotizacion'];//Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }

    $_SESSION['piePromesaClienteCotizacion'] = number_format($_SESSION['piePromesaClienteCotizacion'], 2, '.', '');
    $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['saldoClienteCotizacion'] - $_SESSION['piePromesaClienteCotizacion'] - $_SESSION['reservaClienteCotizacion'];
    $_SESSION['pieCuotasClienteCotizacion'] = number_format($_SESSION['pieCuotasClienteCotizacion'], 2, '.', '');


    //Info desde query
    $_SESSION['fechaEntregaClienteCotizacion'] = $_POST['fechaEntregaClienteCotizacion'];
    $_SESSION['cotiza1ClienteCotizacion'] = $_POST['cotiza1ClienteCotizacion'];

    $_SESSION['cotiza2ClienteCotizacion'] = $_POST['cotiza2ClienteCotizacion'];

    $temp = $_SESSION['cotiza2ClienteCotizacion'];

    $cotiza2Array = explode("|", $temp);

    $_SESSION['cotiza2ClienteCotizacion'] = '';

    for($i = 0; $i < count($cotiza2Array); $i++){
      if($i == 0){
        $_SESSION['cotiza2ClienteCotizacion'] = $_SESSION['cotiza2ClienteCotizacion'] . $cotiza2Array[$i];
      }
      else{
        $_SESSION['cotiza2ClienteCotizacion'] = $_SESSION['cotiza2ClienteCotizacion'] . ' ' . $cotiza2Array[$i];
      }
    }

    $_SESSION['cotiza3ClienteCotizacion'] = $_POST['cotiza3ClienteCotizacion'];

    $temp2 = $_SESSION['cotiza3ClienteCotizacion'];

    $cotiza3Array = explode("|", $temp2);

    $_SESSION['cotiza3ClienteCotizacion'] = '';

    for($j = 0; $j < count($cotiza3Array); $j++){
      if($j == 0){
        $_SESSION['cotiza3ClienteCotizacion'] = $_SESSION['cotiza3ClienteCotizacion'] . $cotiza3Array[$j];
      }
      else{
        $_SESSION['cotiza3ClienteCotizacion'] = $_SESSION['cotiza3ClienteCotizacion'] . ' ' . $cotiza3Array[$j];
      }
    }

    $_SESSION['tasaClienteCotizacion'] = $_POST['tasaClienteCotizacion'];

    $_SESSION['pago15ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (15*12));
    $_SESSION['pago20ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (20*12));
    $_SESSION['pago25ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (25*12));
    $_SESSION['pago30ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (30*12));

    $_SESSION['vendedorClienteCotizacion'] = $_SESSION['nombreUser'];
  }
  else if($_POST['accionClienteCotizacion'] == "Arriendo"){
    //Datos desde query
    $_SESSION['logoProyectoCotizacion'] = '../' . $_POST['logoProyectoCotizacion'];
    if($_SESSION['logoProyectoCotizacion'] == '../'){
      $_SESSION['logoProyectoCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
    }
    $_SESSION['direccionProyectoCotizacion'] = $_POST['direccionProyectoCotizacion'];
    $_SESSION['fechaCotizacion'] = date('d-m-Y');

    //Desde post
    $_SESSION['nombreClienteCotizacion'] = $_POST['nombreClienteCotizacion'];
    $_SESSION['apellidoClienteCotizacion'] = $_POST['apellidoClienteCotizacion'];
    $_SESSION['rutClienteCotizacion'] = $_POST['rutClienteCotizacion'];
    $_SESSION['celularClienteCotizacion'] = $_POST['celularClienteCotizacion'];
    $_SESSION['mailClienteCotizacion'] = $_POST['mailClienteCotizacion'];
    $_SESSION['departaentoClienteCotizacion'] = $_POST['departaentoClienteCotizacion'];
    $_SESSION['fono1ClienteCotizacion'] = $_POST['fono1ClienteCotizacion'];
    $_SESSION['emailInmoClienteCotizacion'] = $_POST['emailInmoClienteCotizacion'];
    $_SESSION['webInmoClienteCotizacion'] = $_POST['webInmoClienteCotizacion'];

    //Query desde unidad
    $_SESSION['tipologiaClienteCotizacion'] = $_POST['tipologiaClienteCotizacion'];
    $_SESSION['modeloClienteCotizacion'] = $_POST['modeloClienteCotizacion'];
    $_SESSION['orientacionClienteCotizacion'] = $_POST['orientacionClienteCotizacion'];
    $_SESSION['m2UtilesClienteCotizacion'] = $_POST['m2UtilesClienteCotizacion'];
    $_SESSION['m2TerrazaClienteCotizacion'] = $_POST['m2TerrazaClienteCotizacion'];
    $_SESSION['m2TotalClienteCotizacion'] = $_POST['m2TotalClienteCotizacion'];

    //Desde post array
    if($_POST['estacionamientosClienteCotizacion'] == null){
      $_SESSION['estacionamientosClienteCotizacion'] = $_POST['cantEstacionamientos'];
    }
    else{
      $_SESSION['estacionamientosClienteCotizacion'] = $_POST['cantEstacionamientos'] + count($_POST['estacionamientosClienteCotizacion']);
    }
    if($_POST['bodegasClienteCotizacion'] == null){
      $_SESSION['bodegasClienteCotizacion'] = $_POST['cantBodegas'];
    }
    else{
      $_SESSION['bodegasClienteCotizacion'] = $_POST['cantBodegas'] + count($_POST['bodegasClienteCotizacion']);
    }

    //Valor de la uf
    $ch = curl_init();

    $fecha = new Datetime();
    $dia = $fecha->format('d');
		$mes = $fecha->format('m');
		$ano = $fecha->format('Y');

		$fecha = $fecha->format('d-m-Y');

	 // set url

 		 $valor = $_POST['valorUFJqueryHoy'];

    $_SESSION['ufClienteCotizacion'] = $valor;

    //Datos desde query
    $_SESSION['departamentoUfClienteCotizacion'] = $_POST['departamentoUfClienteCotizacion'];
    $_SESSION['bodegaUfClienteCotizacion'] = 0;
    $_SESSION['estacionamientoUfClienteCotizacion'] = 0;

    //Datos desde post
    $_SESSION['descuento1ClienteCotizacion'] = $_POST['descuento1ClienteCotizacion'];
    $_SESSION['descuento2ClienteCotizacion'] = $_POST['descuento2ClienteCotizacion'];

    //Calculos (simil venta de momento)
    $_SESSION['total2UFClienteCotizacion'] = $_SESSION['departamentoUfClienteCotizacion'] - ($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['descuento1ClienteCotizacion']/100);

    $_SESSION['totalFUFClienteCotizacion'] = ($_SESSION['departamentoUfClienteCotizacion']+$_SESSION['bodegaUfClienteCotizacion']+$_SESSION['estacionamientoUfClienteCotizacion']) -
    ($_SESSION['total2UFClienteCotizacion']*$_SESSION['descuento2ClienteCotizacion']/100) -
    ($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['descuento1ClienteCotizacion']/100);

    $_SESSION['totalFUFClienteCotizacion'] = number_format($_SESSION['totalFUFClienteCotizacion'],2,'.','');

    $_SESSION['bonoVentaCrearCotizacion'] = $_POST['bonoVentaCrearCotizacion'];
    if($_SESSION['departamentoUfClienteCotizacion'] != 0){
        $_SESSION['totalFUFClienteCotizacion']  = $_SESSION['totalFUFClienteCotizacion'] - $_POST['bonoVentaCrearCotizacion'];
    }

    //Dato desde query
    $_SESSION['imgPlanoClienteCotizacion'] = '../' . $_POST['imgPlanoClienteCotizacion'];
    if($_SESSION['imgPlanoClienteCotizacion'] == '../' || $_POST['imgPlanoClienteCotizacion'] == '../view/img/imagenes_cliente/proyectos/PAT/unidad/plano/' || strlen($_POST['imgPlanoClienteCotizacion']) < 65){
      $_SESSION['imgPlanoClienteCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
    }

    //Formas de pago - Datos desde post
    $_SESSION['reservaClienteCotizacion'] = $_POST['reservaClienteCotizacion']; //UF
    //Calculo de pie promesa
    if($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100 > $_SESSION['reservaClienteCotizacion']){
      $_SESSION['piePromesaClienteCotizacion'] = ($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100)-$_POST['reservaClienteCotizacion'];

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['saldoClienteCotizacion'] = $_POST['saldoClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }
    else{
      $_SESSION['piePromesaClienteCotizacion'] = 0;

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      if(($_SESSION['pieCuotasClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'] + ($_SESSION['reservaClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'])) > 100){
        $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - ($_SESSION['reservaClienteCotizacion']);
      }
      $_SESSION['saldoClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['pieCuotasClienteCotizacion'] -  $_SESSION['reservaClienteCotizacion'];//Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }

    $_SESSION['piePromesaClienteCotizacion'] = number_format($_SESSION['piePromesaClienteCotizacion'], 2, '.', '');$_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['saldoClienteCotizacion'] - $_SESSION['piePromesaClienteCotizacion'] - $_SESSION['reservaClienteCotizacion'];
    $_SESSION['pieCuotasClienteCotizacion'] = number_format($_SESSION['pieCuotasClienteCotizacion'], 2, '.', '');

    //Info desde query
    $_SESSION['fechaEntregaClienteCotizacion'] = $_POST['fechaEntregaClienteCotizacion'];
    $_SESSION['cotiza1ClienteCotizacion'] = $_POST['cotiza1ClienteCotizacion'];

    $_SESSION['cotiza2ClienteCotizacion'] = $_POST['cotiza2ClienteCotizacion'];

    $temp = $_SESSION['cotiza2ClienteCotizacion'];

    $cotiza2Array = explode("|", $temp);

    $_SESSION['cotiza2ClienteCotizacion'] = '';

    for($i = 0; $i < count($cotiza2Array); $i++){
      if($i == 0){
        $_SESSION['cotiza2ClienteCotizacion'] = $_SESSION['cotiza2ClienteCotizacion'] . $cotiza2Array[$i];
      }
      else{
        $_SESSION['cotiza2ClienteCotizacion'] = $_SESSION['cotiza2ClienteCotizacion'] . ' ' . $cotiza2Array[$i];
      }
    }

    $_SESSION['cotiza3ClienteCotizacion'] = $_POST['cotiza3ClienteCotizacion'];

    $temp2 = $_SESSION['cotiza3ClienteCotizacion'];

    $cotiza3Array = explode("|", $temp2);

    $_SESSION['cotiza3ClienteCotizacion'] = '';

    for($j = 0; $j < count($cotiza3Array); $j++){
      if($j == 0){
        $_SESSION['cotiza3ClienteCotizacion'] = $_SESSION['cotiza3ClienteCotizacion'] . $cotiza3Array[$j];
      }
      else{
        $_SESSION['cotiza3ClienteCotizacion'] = $_SESSION['cotiza3ClienteCotizacion'] . ' ' . $cotiza3Array[$j];
      }
    }

    $_SESSION['tasaClienteCotizacion'] = $_POST['tasaClienteCotizacion'];

    $_SESSION['pago15ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (15*12));
    $_SESSION['pago20ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (20*12));
    $_SESSION['pago25ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (25*12));
    $_SESSION['pago30ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (30*12));

    $_SESSION['vendedorClienteCotizacion'] = $_SESSION['nombreUser'];
  }
  else if($_POST['accionClienteCotizacion'] == "Leasing"){
    //Datos desde query
    $_SESSION['logoProyectoCotizacion'] = '../' . $_POST['logoProyectoCotizacion'];
    if($_SESSION['logoProyectoCotizacion'] == '../'){
      $_SESSION['logoProyectoCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
    }
    $_SESSION['direccionProyectoCotizacion'] = $_POST['direccionProyectoCotizacion'];
    $_SESSION['fechaCotizacion'] = date('d-m-Y');

    //Desde post
    $_SESSION['nombreClienteCotizacion'] = $_POST['nombreClienteCotizacion'];
    $_SESSION['apellidoClienteCotizacion'] = $_POST['apellidoClienteCotizacion'];
    $_SESSION['rutClienteCotizacion'] = $_POST['rutClienteCotizacion'];
    $_SESSION['celularClienteCotizacion'] = $_POST['celularClienteCotizacion'];
    $_SESSION['mailClienteCotizacion'] = $_POST['mailClienteCotizacion'];
    $_SESSION['departaentoClienteCotizacion'] = $_POST['departaentoClienteCotizacion'];
    $_SESSION['fono1ClienteCotizacion'] = $_POST['fono1ClienteCotizacion'];
    $_SESSION['emailInmoClienteCotizacion'] = $_POST['emailInmoClienteCotizacion'];
    $_SESSION['webInmoClienteCotizacion'] = $_POST['webInmoClienteCotizacion'];

    //Query desde unidad
    $_SESSION['tipologiaClienteCotizacion'] = $_POST['tipologiaClienteCotizacion'];
    $_SESSION['modeloClienteCotizacion'] = $_POST['modeloClienteCotizacion'];
    $_SESSION['orientacionClienteCotizacion'] = $_POST['orientacionClienteCotizacion'];
    $_SESSION['m2UtilesClienteCotizacion'] = $_POST['m2UtilesClienteCotizacion'];
    $_SESSION['m2TerrazaClienteCotizacion'] = $_POST['m2TerrazaClienteCotizacion'];
    $_SESSION['m2TotalClienteCotizacion'] = $_POST['m2TotalClienteCotizacion'];

    //Desde post array
    if($_POST['estacionamientosClienteCotizacion'] == null){
      $_SESSION['estacionamientosClienteCotizacion'] = $_POST['cantEstacionamientos'];
    }
    else{
      $_SESSION['estacionamientosClienteCotizacion'] = $_POST['cantEstacionamientos'] + count($_POST['estacionamientosClienteCotizacion']);
    }
    if($_POST['bodegasClienteCotizacion'] == null){
      $_SESSION['bodegasClienteCotizacion'] = $_POST['cantBodegas'];
    }
    else{
      $_SESSION['bodegasClienteCotizacion'] = $_POST['cantBodegas'] + count($_POST['bodegasClienteCotizacion']);
    }

    //Valor de la uf
    $ch = curl_init();

    $fecha = new Datetime();
    $dia = $fecha->format('d');
		$mes = $fecha->format('m');
		$ano = $fecha->format('Y');

		$fecha = $fecha->format('d-m-Y');

 		  $valor = $_POST['valorUFJqueryHoy'];

    $_SESSION['ufClienteCotizacion'] = $valor;

    //Datos desde query
    $_SESSION['departamentoUfClienteCotizacion'] = $_POST['departamentoUfClienteCotizacion'];
    $_SESSION['bodegaUfClienteCotizacion'] = 0;
    $_SESSION['estacionamientoUfClienteCotizacion'] = 0;

    $ValorDeptoVenta = valorUnidadVenta($_POST['codProyectoClienteCotizacion'], $_POST['departaentoClienteCotizacion']);
    $_SESSION['departamentoUfClienteCotizacionVenta'] = $ValorDeptoVenta['VALORUF'];

    //Calculo de valor final de bodegas
    for($i = 0; $i < count($_POST['bodegasClienteCotizacion']); $i++){
      $row = valorUnidad($_POST['bodegasClienteCotizacion'][$i]);
      $_SESSION['bodegaUfClienteCotizacion'] = $_SESSION['bodegaUfClienteCotizacion'] + $row['VALORUF'];
    }

    for($j = 0; $j < count($_POST['estacionamientosClienteCotizacion']); $j++){
      $row2 = valorUnidad($_POST['estacionamientosClienteCotizacion'][$j]);
      $_SESSION['estacionamientoUfClienteCotizacion'] = $_SESSION['estacionamientoUfClienteCotizacion'] + $row2['VALORUF'];
    }

    //Datos desde post
    $_SESSION['descuento1ClienteCotizacion'] = $_POST['descuento1ClienteCotizacion'];
    $_SESSION['descuento2ClienteCotizacion'] = $_POST['descuento2ClienteCotizacion'];

    //Calculos (simil venta de momento)
    $_SESSION['total2UFClienteCotizacion'] = $_SESSION['departamentoUfClienteCotizacion'];

    $_SESSION['totalFUFClienteCotizacion'] = ($_SESSION['departamentoUfClienteCotizacionVenta']+$_SESSION['bodegaUfClienteCotizacion']+$_SESSION['estacionamientoUfClienteCotizacion']) - ($_SESSION['departamentoUfClienteCotizacionVenta']*$_SESSION['descuento1ClienteCotizacion']/100) - (($_SESSION['departamentoUfClienteCotizacionVenta'] - ($_SESSION['departamentoUfClienteCotizacionVenta']*$_SESSION['descuento1ClienteCotizacion']/100))*$_SESSION['descuento2ClienteCotizacion']/100);

    $_SESSION['totalFUFClienteCotizacion'] = number_format($_SESSION['totalFUFClienteCotizacion'],2,'.','');

    $_SESSION['bonoVentaCrearCotizacion'] = $_POST['bonoVentaCrearCotizacion'];
    if($_SESSION['departamentoUfClienteCotizacion'] != 0){
        $_SESSION['totalFUFClienteCotizacion']  = $_SESSION['totalFUFClienteCotizacion'] - $_POST['bonoVentaCrearCotizacion'];
    }

    //Dato desde query
    $_SESSION['imgPlanoClienteCotizacion'] = '../' . $_POST['imgPlanoClienteCotizacion'];
    if($_SESSION['imgPlanoClienteCotizacion'] == '../' || $_POST['imgPlanoClienteCotizacion'] == '../view/img/imagenes_cliente/proyectos/PAT/unidad/plano/' || strlen($_POST['imgPlanoClienteCotizacion']) < 65){
      $_SESSION['imgPlanoClienteCotizacion'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
    }

    //Formas de pago - Datos desde post
    $_SESSION['reservaClienteCotizacion'] = $_POST['reservaClienteCotizacion']+(($_SESSION['departamentoUfClienteCotizacion']*12)*0.65)+$_SESSION['departamentoUfClienteCotizacion']; //UF
    //Calculo de pie promesa
    if($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100 > $_SESSION['reservaClienteCotizacion']){
      $_SESSION['piePromesaClienteCotizacion'] = ($_POST['piePromesaClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100)-$_SESSION['reservaClienteCotizacion'];

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['saldoClienteCotizacion'] = $_POST['saldoClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100; //Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }
    else{
      $_SESSION['piePromesaClienteCotizacion'] = 0;

      $_SESSION['pieCuotasClienteCotizacion'] = $_POST['pieCuotasClienteCotizacion']*$_SESSION['totalFUFClienteCotizacion']/100;
      if(($_SESSION['pieCuotasClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'] + ($_SESSION['reservaClienteCotizacion']*100/$_SESSION['totalFUFClienteCotizacion'])) > 100){
        $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - ($_SESSION['reservaClienteCotizacion']);
      }
      $_SESSION['saldoClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['pieCuotasClienteCotizacion'] -  $_SESSION['reservaClienteCotizacion'];//Porcentaje
      $_SESSION['cuotasClienteCotizacion'] = $_POST['cuotasClienteCotizacion'];
    }

    $_SESSION['piePromesaClienteCotizacion'] = number_format($_SESSION['piePromesaClienteCotizacion'], 2, '.', '');
    // $_SESSION['pieCuotasClienteCotizacion'] = $_SESSION['totalFUFClienteCotizacion'] - $_SESSION['saldoClienteCotizacion'] - $_SESSION['piePromesaClienteCotizacion'] - $_SESSION['reservaClienteCotizacion'];
    $_SESSION['pieCuotasClienteCotizacion'] = number_format($_SESSION['pieCuotasClienteCotizacion'], 2, '.', '');

    //Info desde query
    $_SESSION['fechaEntregaClienteCotizacion'] = $_POST['fechaEntregaClienteCotizacion'];
    $_SESSION['cotiza1ClienteCotizacion'] = $_POST['cotiza1ClienteCotizacion'];

    $_SESSION['cotiza2ClienteCotizacion'] = $_POST['cotiza2ClienteCotizacion'];

    $temp = $_SESSION['cotiza2ClienteCotizacion'];

    $cotiza2Array = explode("|", $temp);

    $_SESSION['cotiza2ClienteCotizacion'] = '';

    for($i = 0; $i < count($cotiza2Array); $i++){
      if($i == 0){
        $_SESSION['cotiza2ClienteCotizacion'] = $_SESSION['cotiza2ClienteCotizacion'] . $cotiza2Array[$i];
      }
      else{
        $_SESSION['cotiza2ClienteCotizacion'] = $_SESSION['cotiza2ClienteCotizacion'] . ' ' . $cotiza2Array[$i];
      }
    }

    $_SESSION['cotiza3ClienteCotizacion'] = $_POST['cotiza3ClienteCotizacion'];

    $temp2 = $_SESSION['cotiza3ClienteCotizacion'];

    $cotiza3Array = explode("|", $temp2);

    $_SESSION['cotiza3ClienteCotizacion'] = '';

    for($j = 0; $j < count($cotiza3Array); $j++){
      if($j == 0){
        $_SESSION['cotiza3ClienteCotizacion'] = $_SESSION['cotiza3ClienteCotizacion'] . $cotiza3Array[$j];
      }
      else{
        $_SESSION['cotiza3ClienteCotizacion'] = $_SESSION['cotiza3ClienteCotizacion'] . ' ' . $cotiza3Array[$j];
      }
    }

    $_SESSION['tasaClienteCotizacion'] = $_POST['tasaClienteCotizacion'];

    $_SESSION['pago15ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (15*12));
    $_SESSION['pago20ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (20*12));
    $_SESSION['pago25ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (25*12));
    $_SESSION['pago30ClienteCotizacion'] = pago($_SESSION['tasaClienteCotizacion'], $_SESSION['saldoClienteCotizacion'], (30*12));

    $_SESSION['vendedorClienteCotizacion'] = $_SESSION['nombreUser'];
  }
}

$_SESSION['saldoClienteCotizacion'] = number_format($_SESSION['saldoClienteCotizacion'], 2, '.', '');
?>
