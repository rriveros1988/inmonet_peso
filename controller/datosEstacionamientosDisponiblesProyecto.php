<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');

	if(count($_POST) > 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$codigoCotiza = $_POST['numeroCotizacion'];
    	$row = consultaEstacionamientosDisponiblesProyecto($codigoProyecto,$codigoCotiza);

        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){
                      $return = $return . $row[$i]['IDUNIDAD'] . ',' . $row[$i]['CODIGO'] . ',' . $row[$i]['VALORUF'];
                    }
                    else{
                      $return = $return . ',' .  $row[$i]['IDUNIDAD'] . ',' . $row[$i]['CODIGO'] . ',' . $row[$i]['VALORUF'];
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>
