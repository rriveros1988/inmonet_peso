<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) == 0){
			$rutUsuario = $_SESSION['rutUser'];
			if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
				$row = consultaProyectosNoDisponilesAdmin();
			}
			else if($_SESSION['idperfil'] == 4){
				$row = consultaProyectosNoDisponilesCli($rutUsuario);
			}
			else{
				$row = consultaProyectosNoDisponiles($rutUsuario);
			}

        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){

                      $return = $return . $row[$i]['CODIGOPROYECTO'] . ',' . $row[$i]['NOMBRE'] . ',' . $row[$i]['INMOBILIARIA'] . ',' . $row[$i]['ESTADO'];
                    }
                    else{

                      $return = $return . ',' . $row[$i]['CODIGOPROYECTO'] . ',' . $row[$i]['NOMBRE'] . ',' . $row[$i]['INMOBILIARIA'] . ',' . $row[$i]['ESTADO'];
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>
