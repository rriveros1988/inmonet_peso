<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
	require("phpmailer/PHPMailerAutoload.php");

  date_default_timezone_set('America/Santiago');

	if(count($_POST) > 0){
    $rutCrearUsuario = str_replace(".","",$_POST['rutCrearUsuario']);
    $apellidosCrearUsuario = $_POST['apellidosCrearUsuario'];
    $nombresCrearUsuario = $_POST['nombresCrearUsuario'];
    $emailCrearUsuario = $_POST['emailCrearUsuario'];
    $fonoCrearUsuario = $_POST['fonoCrearUsuario'];
    $perfilCrearUsuario = $_POST['perfilCrearUsuario'];
    $passCrearUsuario = $_POST['passCrearUsuario'];

    if($fonoCrearUsuario == ''){
      $fonoCrearUsuario = 0;
    }

    $row = ingresaUsuario($rutCrearUsuario,$apellidosCrearUsuario,$nombresCrearUsuario,$emailCrearUsuario,$fonoCrearUsuario,$perfilCrearUsuario,md5($passCrearUsuario));

    if($row == "Ok")
    {
      //instancio un objeto de la clase PHPMailer
      $mail = new PHPMailer(); // defaults to using php "mail()"

      //Codificacion
      $mail->CharSet = 'UTF-8';

      //indico a la clase que use SMTP
      $mail->IsSMTP();
      //Debo de hacer autenticación SMTP
      $mail->SMTPAuth = true;
      //indico el servidor SMTP
      $mail->Host = "mail.inmonet.cl";
      //indico el puerto que usa Gmail
      $mail->Port = 465;
      //indico un usuario / clave de un usuario
      $mail->Username = "no-reply@inmonet.cl";
      $mail->Password = "inmonet2018";

      $mail->SMTPSecure = 'ssl';

      $mail->SMTPOptions = array(
	        'ssl' => array(
	            'verify_peer' => false,
	            'verify_peer_name' => false,
	            'allow_self_signed' => true
	        )
	    );

      $firma = "LivingNet
                <br />
                Gestión inmobiliaria
                <br />
                ..........................................................................................................................................................................
                <br>
                <br>
                AVISO LEGAL.
                <br>
                <font style='margin-top: 0; line-height: 15px;font-family: Arial;font-size:7.5pt; text-align: justify; width: 100%'>
                Este mensaje y sus documentos anexos pueden contener información confidencial o legalmente protegida. Está dirigido única y exclusivamente a la persona o entidad reseñada como destinatarios del mensaje. Si este mensaje le hubiera llegado por error, por favor elimínelo sin revisarlo ni reenviarlo y notifíquelo lo antes posible al remitente. Cualquier divulgación, copia o utilización de dicha información es contraria a la ley. Le agradecemos su colaboración.
                </font>
                <br>";

        //$mail->AddEmbeddedImage('../view/img/logos/living_logo_mail.png', 'firmaPng', 'firmaPng.png');

        $body = "<div style='width: 100%; text-align: justify; margin: 0 auto;'>
		    <font style='font-size: 14px;'>
		    Estimado " . $nombresCrearUsuario . " " . $apellidosCrearUsuario . ",
		    <br />
		    <br />
		    Le informamos que se ha creado una cuenta a su nombre en el sistema InmoNet Gestión Inmobiliaria.<br /><br />
        A continuación le indicamos sus credenciales de acceso:<br /><br />
        URL: <a href='https://www.inmonet.cl'>www.inmonet.cl</a>
		    <br />
        Usuario: " . $rutCrearUsuario . "
        <br />
        Contraseña: " . $passCrearUsuario . "
		    <br />
        <br />
		    </font>
		    <div'>
		        <font style='font-size: 14px;'>
		            Saludos cordiales.
		        </font>
		        <br />
		        <br />
		        " . $firma . "
		    </div>
		    ";

        $mail->SetFrom('no-reply@inmonet.cl', "Alertas InmoNet");

		    //defino la dirección de email de "reply", a la que responder los mensajes
		    //Obs: es bueno dejar la misma dirección que el From, para no caer en spam
		    $mail->AddReplyTo('no-reply@inmonet.cl', "Alertas InmoNet");
		    //Defino la dirección de correo a la que se envía el mensaje

		    $listaMails = array($emailCrearUsuario);

        //Agregamos destinatarios
		    for($i = 0; $i < count($listaMails); $i++){
		        $mail->AddAddress($listaMails[$i], $listaMails[$i]);
		    }

        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

		    $fecha = strtotime('+0 day');
    		$fecha = $dias[date('w', $fecha)]." ".date('d', $fecha)." de ".$meses[date('n', $fecha)-1]. " ".date('Y', $fecha) . " a las " . date('h:i:s A', $fecha);

        $mail->Subject = "Creación de cuenta " . $fecha . "";

		    //Puedo definir un cuerpo alternativo del mensaje, que contenga solo texto
		    $mail->AltBody = "Creación de cuenta " . $fecha . "";

		    //inserto el texto del mensaje en formato HTML
		    $mail->MsgHTML($body);

        //envío el mensaje, comprobando si se envió correctamente
		    if($mail->Send()) {
		        echo "Ok";
		    }
		    else{
		    	//echo $mail->ErrorInfo;
          echo "Sin datos";
		    }
    }
    else{
      //echo $row;
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>
