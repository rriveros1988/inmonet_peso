<?php
  //ini_set('display_errors', 'On');
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');

  if(count($_POST) > 0){
    // $document = '/var/www/html/Git/inmonet';
    // $document = '/home/livingne/inmonet.cl/test';
    $document = '/home/livingne/inmonet.cl';

    if(!is_dir("../repositorio/" . $_POST['codigoProyecto'])){
      mkdir("../repositorio/" . $_POST['codigoProyecto'], 0777);
      mkdir("../repositorio/" . $_POST['codigoProyecto'] . "/reserva", 0777);
    }
    if(!is_dir("../repositorio/" . $_POST['codigoProyecto'] . "/reserva")){
      mkdir("../repositorio/" . $_POST['codigoProyecto'] . "/reserva", 0777);
    }

    $row = consultaReservaEspecificaCargaDocs($_POST['codigoProyecto'], $_POST['numeroOperacion']);

    $rutaFicha = $document . '/repositorio/' . $row[0]['FICHA_PDF'];
    $rutaCO = $document . '/repositorio/' . $row[0]['COTIZA_PDF'];
    $rutaRCompra = $document . '/repositorio/' . $row[0]['RCOMPRA_PDF'];
    $rutaCI = $document . '/repositorio/' . $row[0]['CI_PDF'];
    $rutaPB = $document . '/repositorio/' . $row[0]['PB_PDF'];
    $rutaCR = $document . '/repositorio/' . $row[0]['CR_PDF'];
    $rutaCC = $document . '/repositorio/' . $row[0]['CC_PDF'];

    //Crga Ficha
    $fichaDoc = $_FILES['fichaReservaDoc']['name'];
    $fichaExt = pathinfo($fichaDoc, PATHINFO_EXTENSION);

    if($fichaExt != ''){
      if($rutaFicha != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['fichaReservaDoc']['tmp_name'],$rutaFicha);
      }
      else{
        $rutaFicha = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_FC.pdf';
        move_uploaded_file($_FILES['fichaReservaDoc']['tmp_name'],$rutaFicha);
      }
    }

    //Crga Cotizacion
    $coDoc = $_FILES['cotizacionDoc']['name'];
    $coExt = pathinfo($coDoc, PATHINFO_EXTENSION);

    if($coExt != ''){
      if($rutaCO != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['cotizacionDoc']['tmp_name'],$rutaCO);
      }
      else{
        $rutaCO = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/cotizacion/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '.pdf';
        move_uploaded_file($_FILES['cotizacionDoc']['tmp_name'],$rutaCO);
      }
    }

    //Carga RCompra
    $rcompraDoc = $_FILES['rcompraReservaDoc']['name'];
    $rcompraExt = pathinfo($rcompraDoc, PATHINFO_EXTENSION);

    if($rcompraExt != ''){
      if($rutaRCompra != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['rcompraReservaDoc']['tmp_name'],$rutaRCompra);
      }
      else{
        $rutaRCompra = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_RE.pdf';
        move_uploaded_file($_FILES['rcompraReservaDoc']['tmp_name'],$rutaRCompra);
      }
    }

    //Carga CI
    $ciDoc = $_FILES['ciReservaDoc']['name'];
    $ciExt = pathinfo($ciDoc, PATHINFO_EXTENSION);

    if($ciExt != ''){
      if($rutaCI != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['ciReservaDoc']['tmp_name'],$rutaCI);
      }
      else{
        $rutaCI = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CI.pdf';
        move_uploaded_file($_FILES['ciReservaDoc']['tmp_name'],$rutaCI);
      }
    }

    //Carga PB
    $pbDoc = $_FILES['pbReservaDoc']['name'];
    $pbExt = pathinfo($pbDoc, PATHINFO_EXTENSION);

    if($pbExt != ''){
      if($rutaPB != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
      else{
        $rutaPB = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PB.pdf';
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
    }

    //Carga PB
    $pbDoc = $_FILES['pbReservaDoc']['name'];
    $pbExt = pathinfo($pbDoc, PATHINFO_EXTENSION);

    if($pbExt != ''){
      if($rutaPB != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
      else{
        $rutaPB = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PB.pdf';
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
    }

    //Carga CC
    $ccDoc = $_FILES['ccReservaDoc']['name'];
    $ccExt = pathinfo($ccDoc, PATHINFO_EXTENSION);

    if($ccExt != ''){
      if($rutaCC != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['ccReservaDoc']['tmp_name'],$rutaCC);
      }
      else{
        $rutaCC = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CC.pdf';
        move_uploaded_file($_FILES['ccReservaDoc']['tmp_name'],$rutaCC);
      }
    }

    //Carga CR
    $crDoc = $_FILES['crReservaDoc']['name'];
    $crExt = pathinfo($crDoc, PATHINFO_EXTENSION);

    if($crExt != ''){
      if($rutaCR != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['crReservaDoc']['tmp_name'],$rutaCR);
      }
      else{
        $rutaCR = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CR.pdf';
        move_uploaded_file($_FILES['crReservaDoc']['tmp_name'],$rutaCR);
      }
    }

    //Local e inmonet
    //actualizaRutasDocumentosReserva(substr($rutaFicha,38,strlen($rutaFicha)),substr($rutaRCompra,38,strlen($rutaRCompra)),substr($rutaCI,38,strlen($rutaCI)),substr($rutaPB,38,strlen($rutaPB)),substr($rutaCC,38,strlen($rutaCC)),substr($rutaCR,38,strlen($rutaCR)),substr($rutaCO,38,strlen($rutaCO)),$row[0]['IDRESERVA']);
    //Inmonet Test
    //
    actualizaRutasDocumentosReserva(substr($rutaFicha,38,strlen($rutaFicha)),substr($rutaRCompra,38,strlen($rutaRCompra)),substr($rutaCI,38,strlen($rutaCI)),substr($rutaPB,38,strlen($rutaPB)),substr($rutaCC,38,strlen($rutaCC)),substr($rutaCR,38,strlen($rutaCR)),substr($rutaCO,38,strlen($rutaCO)),$row[0]['IDRESERVA']);

    $documentos = "";
    if($fichaDoc != NULL ){
      $documentos .= "Ficha cliente, ";
    }
    if($coDoc != NULL ){
      $documentos .= "Cotizacion, ";
    }
    if($rcompraDoc != NULL ){
      $documentos .= "Ficha reserva, ";
    }
    if($ciDoc != NULL ){
      $documentos .= "Cedula de identidad, ";
    }
    if($pbDoc != NULL ){
      $documentos .= "Preaprobacion bancaria, ";
    }
    if($ccDoc != NULL ){
      $documentos .= "Carpeta cliente, ";
    }
    if($crDoc != NULL ){
      $documentos .= "Comprobante, ";
    }
    $documentos = trim($documentos, ', ');
    echo "Ok¬" . $_POST['codigoProyecto'] . "¬" .$_POST['numeroOperacion'] . "¬" . $documentos;
  }
?>
