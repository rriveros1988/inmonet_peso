<?php
// ini_set('display_errors', 'On');
require('../src/jpgraph.php');
require('../src/jpgraph_bar.php');
require('../../model/consultas.php');
date_default_timezone_set('America/Santiago');
session_start();

$mes = $_GET['mes'];
$ano = $_GET['ano'];
$codigoProyecto = $_GET['codigoProyecto'];
$accion = $_GET['accion'];
$datosInforme1Reservas = datosInforme1Reservas($accion, $mes, $ano, $codigoProyecto);
$datosProyecto = consultaDatosProyecto($codigoProyecto);


$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$mesesCortos = array("ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic");

$fechasRango = array();
$fechasRangoIndex = array();

if($mes == 12){
	$fechaMax = new Datetime(($ano + 1) . '-' . '01-01');
}
else{
	$fechaMax = new Datetime($ano . '-' . ($mes + 1) . '-01');
}
for($i = 0; $i < 12; $i++){
	$f = strtotime(($i - 12) . 'month', strtotime($fechaMax->format('y-m-d')));
	$m =  date('m',$f);
	$y =  date('y',$f);
	$mText = $mesesCortos[$m-1];
	$fFinal = $mText . "-" . $y;
	$fechasRango[] = $fFinal;
	$fechasRangoIndex[] = $y . '_' . $m;
}

$datos = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos2 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos3 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$labels = $fechasRango;
$tam = 0;

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme1Reservas[0])){
		$datos[$i-1] = $datosInforme1Reservas[0][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	$datos2[$i-1] = $datosInforme1Reservas[1][$i];
}

for($i = 1; $i < 13; $i++){
  if($tam < ($datosInforme1Reservas[2][$i] + $datosInforme1Reservas[1][$i] + $datosInforme1Reservas[0][$i])){
		$tam = ($datosInforme1Reservas[2][$i] + $datosInforme1Reservas[1][$i] + $datosInforme1Reservas[0][$i]);
	}
	$datos3[$i-1] = $datosInforme1Reservas[2][$i];
}

$grafico = new Graph(800, 260, 'auto');
$grafico->img->SetMargin(40,10,10,200);
$grafico->SetScale("textint",0,$tam+40);
// $grafico->title->SetFont(FF_ARIAL,FS_BOLD,12);
// $grafico->title->Set('Altas por agencia');
$grafico->xaxis->SetTickLabels($labels);
$grafico->xaxis->SetLabelAngle(0);
// $grafico->xaxis->SetFont(FF_ARIAL,FS_BOLD,10);
// $grafico->yaxis->SetFont(FF_ARIAL,FS_BOLD,10);
$grafico->yaxis->SetWeight(2);
$grafico->xaxis->SetWeight(2);
$barplot1 = new barPlot($datos);

// Setup the values that are displayed on top of each bar
$barplot1->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barplot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot1->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot1->value->SetColor("#8e8e8e","#8e8e8e");

// Un gradiente Horizontal de morados
$barplot1->SetFillGradient("#8e8e8e", "#8e8e8e", GRAD_HOR);

// $barplot1->SetColor("black");

// 30 pixeles de ancho para cada barra
$barplot1->SetWidth(25);

// Join them in an accumulated (stacked) plot
$accbplot = new AccBarPlot(array($barplot1));
$grafico->legend->SetFrameWeight(1);
$grafico->legend->SetColumns(3);
$grafico->legend->SetPos(0.5,0.96,'center','bottom');
$grafico->legend->SetColor('#8e8e8e','#8e8e8e');

$grafico->Add($accbplot);

$barplot1->SetFillColor("#8e8e8e");
$barplot1->SetLegend('Reservas');
// //Show numero
// $barplot1->value->SetFormat('%d');
// $barplot1->value->Show();
// $barplot1->value->SetColor('white');
//
// $barplot2->value->SetFormat('%d');
// $barplot2->value->Show();
// $barplot2->value->SetColor('black');
//
// $barplot3->value->SetFormat('%d');
// $barplot3->value->Show();
// $barplot3->value->SetColor('white');

$grafico->Stroke();

?>
