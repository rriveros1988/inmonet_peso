<?php
// ini_set('display_errors', 'On');
require('../src/jpgraph.php');
require('../src/jpgraph_bar.php');
require('../../model/consultas.php');
date_default_timezone_set('America/Santiago');
session_start();

$ano = $_GET['ano'];
$mes = $_GET['mes'];
$codigoProyecto = $_GET['codigoProyecto'];
$accion = $_GET['accion'];
$datosInforme1NivelInteres = datosInforme1NivelInteres($accion, $ano, $mes, $codigoProyecto);
$nivel = array(5,4,3,2,1);
$valor = array(0,0,0,0,0);
for($j = 0; $j < count($datosInforme1NivelInteres); $j++){
	for($k = 0; $k < count($nivel); $k++){
		if($nivel[$k] == $datosInforme1NivelInteres[$j][0]){
			$valor[$k] = $datosInforme1NivelInteres[$j][1];
		}
	}
}

$datos = $valor;
$labels = array('P 5','P 4','P 3','P 2','P 1');
$tam = 0;

for($i = 1; $i < count($valor); $i++){
	if($tam < $valor[$i]){
		$tam = $valor[$i];
	}
}

$grafico = new Graph(600, 240, 'auto');
$grafico->img->SetMargin(40,120,10,0);
$grafico->SetScale("textint",0,$tam+20);
// $grafico->title->SetFont(FF_ARIAL,FS_BOLD,12);
// $grafico->title->Set('Altas por agencia');
$grafico->xaxis->SetTickLabels($labels);
$grafico->xaxis->SetLabelAngle(0);
// $grafico->xaxis->SetFont(FF_ARIAL,FS_BOLD,10);
// $grafico->yaxis->SetFont(FF_ARIAL,FS_BOLD,10);
$grafico->yaxis->SetWeight(2);
$grafico->xaxis->SetWeight(2);
$barplot1 = new barPlot($datos);

// Setup the values that are displayed on top of each bar
$barplot1->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barplot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot1->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot1->value->SetColor("#585858","#585858");

// Un gradiente Horizontal de morados
$barplot1->SetFillGradient("#585858", "#585858", GRAD_HOR);

// $barplot1->SetColor("black");

// 30 pixeles de ancho para cada barra
$barplot1->SetWidth(25);

// Join them in an accumulated (stacked) plot
$accbplot = new AccBarPlot(array($barplot1));
$grafico->legend->SetFrameWeight(1);
$grafico->legend->SetColumns(3);
$grafico->legend->SetPos(0.02,0.4,'right','middle');
$grafico->legend->SetColor('#202020','#636363');
// $grafico->xaxis->SetFont(FF_ARIAL	,FS_NORMAL,9);

$grafico->Add($accbplot);

$barplot1->SetFillColor("#585858");
$barplot1->SetLegend("Cantidad");

// //Show numero
// $barplot1->value->SetFormat('%d');
// $barplot1->value->Show();
// $barplot1->value->SetColor('white');
//
// $barplot2->value->SetFormat('%d');
// $barplot2->value->Show();
// $barplot2->value->SetColor('black');
//
// $barplot3->value->SetFormat('%d');
// $barplot3->value->Show();
// $barplot3->value->SetColor('white');

$grafico->Stroke();

?>
