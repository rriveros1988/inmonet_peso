<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
  session_start();

if(count($_POST) > 0){
  $codigoProyecto = $_POST['codigoProyecto'];
  $numeroOperacion = $_POST['numeroOperacion'];
  $estadoUnidad = $_POST['estadoUnidad'];
  
  if ($estadoUnidad == "Reservado" || $estadoUnidad == "Reservada") {
    $estadoPagoReserva = $_POST['estadoPagoReserva'];
    $consultaPago = consultaPagosReservaContable($codigoProyecto, $numeroOperacion);
    if ($consultaPago[0]['ESTADOPAGORESERVA'] != $estadoPagoReserva) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago reserva", "Actualizacion de pago reserva a " . $estadoPagoReserva, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoReserva($codigoProyecto, $numeroOperacion, $estadoPagoReserva)->query("COMMIT");
    
    echo "Ok";
  }else if($estadoUnidad == "Promesado" || $estadoUnidad == "Promesada"){
    $consultaPago = consultaPagosPromesaContable($codigoProyecto, $numeroOperacion);
    $estadoPagoReserva = $_POST['estadoPagoReserva'];
    if ($consultaPago[0]['ESTADOPAGORESERVA'] != $estadoPagoReserva) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago reserva", "Actualizacion de pago reserva a " . $estadoPagoReserva, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoReserva($codigoProyecto, $numeroOperacion, $estadoPagoReserva)->query("COMMIT");

    $estadoPagoPromesa = $_POST['estadoPagoPromesa'];
    if ($consultaPago[0]['ESTADOPAGOPROMESA'] != $estadoPagoPromesa) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago promesa", "Actualizacion de pago promesa a " . $estadoPagoPromesa, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoPromesa($codigoProyecto, $numeroOperacion, $estadoPagoPromesa)->query("COMMIT");
    
    if(array_key_exists('cuotas_promesa', $_POST) && count($_POST["cuotas_promesa"]) > 0){
      $cuotasPromesa = $_POST["cuotas_promesa"];
      foreach ($cuotasPromesa as $key => $estadoPagoPromesaCuota) {
        $numeroPromesaCuota = $key+1;
        $estadoPagoPromesaCuota = $estadoPagoPromesaCuota;
        $consultaPagoCuota = consultaCuotasPromesaContable($codigoProyecto,$numeroOperacion);
        $ingreso_cuotas = actualizarEstadoPagoPromesaCuota($codigoProyecto, $numeroOperacion, $estadoPagoPromesaCuota, $numeroPromesaCuota);
        if($ingreso_cuotas == "Error"){
          $ingreso_cuotas->query("ROLLBACK");
          break;
          echo "Sin datos";
        }else{
          if ($consultaPagoCuota[$key]['ESTADOPAGOCUOTA'] != $estadoPagoPromesaCuota) {
            ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago cuota promesa", "Actualizacion de pago cuota promesa " . $numeroPromesaCuota . " a " . $estadoPagoPromesaCuota, $codigoProyecto, $numeroOperacion); 
          }
          $ingreso_cuotas->query("COMMIT");
        }
      }
    }  
    echo "Ok";
  }else if($estadoUnidad == "Escriturado" || $estadoUnidad == "Escriturada"){
    $consultaPago = consultaPagosEscrituraContable($codigoProyecto, $numeroOperacion);
    $estadoPagoReserva = $_POST['estadoPagoReserva'];
    if ($consultaPago[0]['ESTADOPAGORESERVA'] != $estadoPagoReserva) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago reserva", "Actualizacion de pago reserva a " . $estadoPagoReserva, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoReserva($codigoProyecto, $numeroOperacion, $estadoPagoReserva)->query("COMMIT");
    $estadoPagoPromesa = $_POST['estadoPagoPromesa'];
    if ($consultaPago[0]['ESTADOPAGOPROMESA'] != $estadoPagoPromesa) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago promesa", "Actualizacion de pago promesa a " . $estadoPagoPromesa, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoPromesa($codigoProyecto, $numeroOperacion, $estadoPagoPromesa)->query("COMMIT");

    $ingreso_cuotas = "Ok";
    if(array_key_exists('cuotas_promesa', $_POST) && count($_POST["cuotas_promesa"]) > 0){
      $cuotasPromesa = $_POST["cuotas_promesa"];
      foreach ($cuotasPromesa as $key => $estadoPagoPromesaCuota) {
        $numeroPromesaCuota = $key+1;
        $estadoPagoPromesaCuota = $estadoPagoPromesaCuota;
        $consultaPagoCuota = consultaCuotasPromesaContable($codigoProyecto,$numeroOperacion);
        $ingreso_cuotas = actualizarEstadoPagoPromesaCuota($codigoProyecto, $numeroOperacion, $estadoPagoPromesaCuota, $numeroPromesaCuota);
        if($ingreso_cuotas == "Error"){
          $ingreso_cuotas->query("ROLLBACK");
          break;
          echo "Sin datos";
        }else{
          if ($consultaPagoCuota[$key]['ESTADOPAGOCUOTA'] != $estadoPagoPromesaCuota) {
            ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago cuota promesa", "Actualizacion de pago cuota promesa " . $numeroPromesaCuota . " a " . $estadoPagoPromesaCuota, $codigoProyecto, $numeroOperacion); 
          }
          $ingreso_cuotas->query("COMMIT");
        }
      }
    }  

    $estadoPagoEscritura = $_POST['estadoPagoEscritura'];
    if ($consultaPago[0]['ESTADOPAGOESCRITURA'] != $estadoPagoEscritura) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago escritura", "Actualizacion de pago escritura a " . $estadoPagoEscritura, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoEscritura($codigoProyecto, $numeroOperacion, $estadoPagoEscritura)->query("COMMIT");

    $estadoPagoResidual = $_POST['estadoPagoResidual'];
    if ($consultaPago[0]['ESTADOPAGOCUOTA'] != $estadoPagoResidual) {
      ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Contable", "Actualizacion pago diferencial", "Actualizacion de pago diferencial a " . $estadoPagoResidual, $codigoProyecto, $numeroOperacion);
    }
    actualizarEstadoPagoResidual($codigoProyecto, $numeroOperacion, $estadoPagoResidual)->query("COMMIT");
    echo "Ok";
  }
}else{
  echo "Sin datos";
}
  
?>
