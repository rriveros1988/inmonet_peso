<?php
  header('Access*Control*Allow*Origin: *');
  require('../model/consultas.php');

  if(count($_POST) >= 0){
      $codigoProyecto = $_POST['codigoProyecto'];
      $rutVendedor = $_POST['rutVendedor'];
      $anoComision = $_POST['anoComision'];
      $mesComision = $_POST['mesComision'];

      $fecha = new DateTime();
      $fecha->setDate($anoComision, $mesComision, 1);
      $ano = $fecha->format('Y');
      $mes = $fecha->format('m');
      $dia = $fecha->format('d');
      
      $fecha = $fecha->format('t-m-Y');
      $valor = $_POST['valorUFJqueryHoy'];
     
      $row = consultaOperacionesPorVendedor($rutVendedor, $codigoProyecto, $mesComision, $anoComision);

        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    $pesosPromesa = $row[$i]['UFPROMESA']*$valor;
                    $pesosEscritura = $row[$i]['UFESCRITURA']*$valor;
                    if($i == 0){
                      $return = $return . utf8_encode($row[$i]['NUMERO']) . '¬' . utf8_encode($row[$i]['DEPTO'])  . '¬' . utf8_encode($row[$i]['EST'])  . '¬' . utf8_encode($row[$i]['BOD']) . '¬' . utf8_encode($row[$i]['VALORNETOUF']) . '¬' . utf8_encode($row[$i]['FECHACOMISIONPROMESA']) . '¬' . utf8_encode($row[$i]['COMISIONPROMESA']) . '¬' . utf8_encode($row[$i]['UFPROMESA']) . '¬' . utf8_encode($pesosPromesa) . '¬' . utf8_encode($row[$i]['ESTADOPROMESA']) . '¬' . utf8_encode($row[$i]['FECHACOMISIONESCRITURA']) . '¬' . utf8_encode($row[$i]['COMISIONESCRITURA']) . '¬' . utf8_encode($row[$i]['UFESCRITURA']) . '¬' . utf8_encode($pesosEscritura) . '¬' . utf8_encode($row[$i]['ESTADOESCRITURA']);
                    }
                    else{
                      $return = $return . '¬' . utf8_encode($row[$i]['NUMERO']) . '¬' . utf8_encode($row[$i]['DEPTO'])  . '¬' . utf8_encode($row[$i]['EST'])  . '¬' . utf8_encode($row[$i]['BOD']) . '¬' . utf8_encode($row[$i]['VALORNETOUF']) . '¬' . utf8_encode($row[$i]['FECHACOMISIONPROMESA']) . '¬' . utf8_encode($row[$i]['COMISIONPROMESA']) . '¬' . utf8_encode($row[$i]['UFPROMESA']) . '¬' . utf8_encode($pesosPromesa) . '¬' . utf8_encode($row[$i]['ESTADOPROMESA']) . '¬' . utf8_encode($row[$i]['FECHACOMISIONESCRITURA']) . '¬' . utf8_encode($row[$i]['COMISIONESCRITURA']) . '¬' . utf8_encode($row[$i]['UFESCRITURA']) . '¬' . utf8_encode($pesosEscritura) . '¬' . utf8_encode($row[$i]['ESTADOESCRITURA']);
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
  }
  else{
    echo "Sin datos";
  }
?>
