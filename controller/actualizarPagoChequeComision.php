<?php
  // ini_set('display_errors', 'On');
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();
	if(count($_POST) > 0){
		$formaPago = $_POST['formaPago'];
		$banco = $_POST['banco'];
		$serie = $_POST['serie'];
		$nro = $_POST['nro'];
		$fechaComision = new DateTime($_POST['fechaCheque']);
		$fechaCheque = $fechaComision->format('Y-m-d');
		$actor = $_POST['actor'];
		$codigoProyecto = $_POST['codigoProyecto'];
		$numeroOperacion = $_POST['numeroOperacion'];
		$valorUFCom = $_POST['valorUFCom'];
		$montoPagoCom  = $_POST['montoPagoCom'];
		$pagoPromesa = actualizarPagoComisionPromesaEmpresa($codigoProyecto, $numeroOperacion, $actor, $formaPago, $banco, $serie, $nro, $fechaCheque, $valorUFCom, $montoPagoCom);

		if ($pagoPromesa == 'Ok') {
			$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
		    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
		    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
		    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
		    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
			$proyecto = consultaProyectoEspecifico($idProyecto);
	        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
	        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
	        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
	        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
			$fechaPromesa = $fecha->format("d-m-Y");
			$_SESSION['promesaFecha'] = $fechaPromesa;
			$_SESSION['numeroOperacion'] = $numeroOperacion;
			$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
	        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
	        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
	        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
	        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
	        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
	        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
	        if ($cliente1[0]['TIPODOMICILIO'] != '') {
		        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
	        }else{
				$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
	        }
	        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
	        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
	        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
	        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
	        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
	        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
	        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
	        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
	        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
	        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
	        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
	        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
	        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
	        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
	        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
	        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
	        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
	        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
	        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
	        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
	        if(count($cuotas_promesa) > 0){
		        $_SESSION['promesaFormaPagoCuota'] = array();

		        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

		        foreach ($cuotasPromesa as $cuota) {
		            
		            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));
		            
		        }
		      }
		      else{
		        unset($_SESSION['cuotasPromesa']);
		      }
	        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
	        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
	        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
		    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
		    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
		    $datosCheque = consultaDatosPagoComisionCorretaje($codigoProyecto, $numeroOperacion, $actor)[0];
		    $_SESSION['MONTOUFCOMISION'] = number_format($datosCheque['VALORUFCOM'], 2, ',', '.');
			$_SESSION['MONTOPESOCOMISION'] = '$ ' . number_format(($datosCheque['MONTOPAGOCOM']), 0, '.', '.');
			$_SESSION['FORMAPAGOCOMISION'] = consultaFormaPagoReservaEspecifica($datosCheque['FORMAPAGO'])[0]['NOMBRE'];
			$_SESSION['BANCOCOMISION'] = $datosCheque['BANCOPROMESA'];
			$_SESSION['SERIECOMISION'] = $datosCheque['SERIECHEQUEPROMESA'];
			$_SESSION['NROCOMISION'] = $datosCheque['NROCHEQUEPROMESA'];
			$fechaPagoComision = new DateTime($datosCheque['FECHAPAGOCOMISION']);
			$fechaMostrar= $fechaPagoComision->format('d-m-Y');
			$_SESSION['FECHAPAGOCOMISION'] = $fechaMostrar;
			echo 'Ok';
			ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision promesa", $codigoProyecto, $numeroOperacion);
		}else{
			echo 'Sin datos';
		}
	}else{
   		echo 'Sin datos';
  	}
?>
