<?php
  //ini_set('display_errors', 'On');
  require('../../model/consultas.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 35px;
      margin-right:35px;
      margin-top: 20px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 45px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 45px;
    }
    #cabecera3Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 400px;
    }
    #cabecera3Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 400px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 0;
      margin: 0;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 9px; font-family: Arial">
    <?php
      session_start();
    ?>
    <table id="tablaOC">
      <tr>
        <td id="cabecera1Izquierda">
          <img src="../../view/img/logos/living_logo.png" style='height: 60px;'>
        </td>
        <td id="cabecera1Derecha">
          <?php
            echo "<img src='" . $_SESSION['reservaLogoProyecto'] . "' style='height: 60px;'>";
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px;">
          Fecha Ficha:
        </td>
        <td style="text-align: center; width: 100px; border: 1px;">
          <?php
            echo $_SESSION['reservaFecha'];
          ?>
        </td>
        <td style="text-align: left; width: 280px;">

        </td>
        <td style="text-align: center; font-size: 14px; width: 200px; border: 1px; padding-bottom: 5px; padding-top: 5px;">
          <b>FICHA CLIENTE</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 12px;">
          <b>1. DATOS DEL CLIENTE</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Nombre / Razón Social:
        </td>
        <td style="text-align: left; width: 300px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaNombreCliente']) . ' ' . strtoupper($_SESSION['reservaApellidoCliente']);
          ?>
        </td>
        <td style="text-align: center; width: 120px; height: 10px;">
          RUT:
        </td>
        <td style="text-align: left; width: 122px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaRutCliente']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Domicilio:
        </td>
        <td style="text-align: left; width: 560px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaDomicilioCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Comuna:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaComunaCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Profesión:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaProfesionCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Ciudad:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaCiudadCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Institución:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaInstitucionCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Región:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaRegionCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Actividad:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaActividadCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          País:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaPaisCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Nacionalidad:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaNacionalidadCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Renta:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaRentaCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Residencia:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaResidenciaCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Teléfono Celular:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaTelefonoCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Sexo:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaSexoCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Email:
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaMailCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 120px; height: 10px; padding-left: 11px;">
          Fecha Nacimiento:
        </td>
        <td style="text-align: left; width: 56px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            $fechaNac = new Datetime($_SESSION['reservaFechaNacCliente1']);
            echo $fechaNac->format('d');
          ?>
        </td>
        <td style="text-align: left; width: 56px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            $fechaNac = new Datetime($_SESSION['reservaFechaNacCliente1']);
            echo $fechaNac->format('m');
          ?>
        </td>
        <td style="text-align: left; width: 56px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            $fechaNac = new Datetime($_SESSION['reservaFechaNacCliente1']);
            echo $fechaNac->format('Y');
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 5px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Estado Civil:
        </td>
        <td style="text-align: center; width: 30px; border: 1px; height: 15px;">
          <?php
            echo strtoupper($_SESSION['reservaEstadoCivilIDCliente1']);
          ?>
        </td>
        <td style="text-align: center; width: 143px; height: 10px;">
          Nivel Educ.:
        </td>
        <td style="text-align: center; width: 30px; border: 1px; height: 15px;">
          <?php
            echo strtoupper($_SESSION['reservaNivelEducCliente1']);
          ?>
        </td>
        <td style="text-align: center; width: 142px; height: 10px;">
          Casa que habita:
        </td>
        <td style="text-align: center; width: 30px; border: 1px; height: 15px;">
          <?php
            echo strtoupper($_SESSION['reservaCasaHabitaCliente1']);
          ?>
        </td>
        <td style="text-align: center; width: 142px; height: 10px;">
          Motivo de compra:
        </td>
        <td style="text-align: center; width: 30px; border: 1px; height: 15px;">
          <?php
            echo strtoupper($_SESSION['reservaMotivoCompraCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 5px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px; vertical-align: top;">
          1. Soltero<br/>
          2. Separado<br/>
          3. Viudo<br/>
          4. Casado Soc. Conyugal<br/>
          5. Casado Sep. Bienes<br/>
          6. Casado Part. Ganancias<br/>
          7. Divorciado
        </td>
        <td style="text-align: center; width: 82px; height: 15px;">

        </td>
        <td style="text-align: left; width: 143px; height: 10px; vertical-align: top;">
          1. Básico<br/>
          2. Medio<br/>
          3. Técnico<br/>
          4. Universitario<br/>
          5. Otro
        </td>
        <td style="text-align: center; width: 22px; height: 15px;">

        </td>
        <td style="text-align: left; width: 142px; height: 10px; vertical-align: top;">
          1. Propia c/Hipoteca<br/>
          2. Propia s/Hipoteca<br/>
          3. Arrendada<br/>
          4. De la Familia<br/>
          5. Extranjero<br/>
          6. Otro
        </td>
        <td style="text-align: left; width: 33px; height: 15px;">

        </td>
        <td style="text-align: left; width: 142px; height: 10px; vertical-align: top;">
          1. Habitacional<br/>
          2. Inversión<br/>
          3. Comercial<br/>
          4. Otro
        </td>
        <td style="text-align: center; width: 30px; height: 15px;">

        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 12px;">
          <b>2. DATOS DE LA UNIDAD</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          <b>Proyecto:</b>
        </td>
        <td style="text-align: left; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <b><?php
            echo strtoupper($_SESSION['reservaNombreProyecto']);
          ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          <?php echo strtoupper($_SESSION['tipoUnidad']); ?> Nro:
        </td>
        <td style="text-align: center; width: 40px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['reservaNumeroDepto']);
          ?>
        </td>
        <td style="text-align: right; width: 281px; height: 10px; padding-left: 11px;">
          <?php echo strtoupper($_SESSION['tipoUnidad']); ?> Precio Lista:&nbsp;&nbsp;&nbsp;&nbsp;UF&nbsp;&nbsp;
        </td>
        <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo number_format($_SESSION['reservaValorDepto'],2,',','.');
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">

        </td>
        <td style="text-align: center; width: 44px; height: 10px; padding-left: 5px;">

        </td>
        <td style="text-align: right; width: 281px; height: 10px; padding-left: 11px;">
          <?php echo strtoupper($_SESSION['tipoUnidad']); ?> Precio c/Desc:&nbsp;&nbsp;&nbsp;&nbsp;UF&nbsp;&nbsp;
        </td>
        <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo number_format($_SESSION['reservaTotal3'],2,',','.');
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          BODEGA Nro:
        </td>
        <td  style="width: 67px;">
          <table>
            <tr>
              <?php
                for($i = 0; $i < count($_SESSION['bodegasClienteReserva']); $i++){
                  if($i%5 == 0){
                    echo '</tr><tr><td style="text-align: center; width: 37px; border: 1px; height: 10px;">' .
                    consultaUnidadEspecificaReserva($_SESSION['bodegasClienteReserva'][$i])[0]['CODIGO']
                    . '</td>';
                  }
                  else {
                    echo '<td style="text-align: center; width: 37px; border: 1px; height: 10px;">' .
                    consultaUnidadEspecificaReserva($_SESSION['bodegasClienteReserva'][$i])[0]['CODIGO']
                    . '</td>';
                  }
                }
              ?>
            </tr>
          </table>
        </td>
        <td style="text-align: right; width: 266px; max-height: 10px; padding-left: 11px;">
          BOD Precio Lista:&nbsp;&nbsp;&nbsp;&nbsp;UF&nbsp;&nbsp;
        </td>
        <td>
          <table style="width: 200px;">
            <tr>
              <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
                <?php
                  echo number_format($_SESSION['reservaValorBod'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          ESTACIONAM. Nro:
        </td>
        <td  style="width: 110px;">
          <table>
            <tr>
              <?php
                for($i = 0; $i < count($_SESSION['estacionamientosClienteReserva']); $i++){
                  if($i%5 == 0){
                    echo '</tr><tr><td style="text-align: center; width: 37px; border: 1px; height: 10px;">' .
                    consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteReserva'][$i])[0]['CODIGO']
                    . '</td>';
                  }
                  else {
                    echo '<td style="text-align: center; width: 37px; border: 1px; height: 10px;">' .
                    consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteReserva'][$i])[0]['CODIGO']
                    . '</td>';
                  }
                }
              ?>
            </tr>
          </table>
        </td>
        <td style="text-align: right; width: 245px; max-height: 10px;">
          ESTAC Precio Lista:&nbsp;&nbsp;&nbsp;&nbsp;UF&nbsp;&nbsp;
        </td>
        <td>
          <table style="width: 200px;">
            <tr>
              <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
                <?php
                  echo number_format($_SESSION['reservaValorEst'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 20px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">

        </td>
        <td style="text-align: center; width: 44px; height: 10px; padding-left: 5px;">

        </td>
        <td style="text-align: right; width: 281px; height: 10px; padding-left: 11px;">
          PRECIO VENTA TOTAL&nbsp;&nbsp;&nbsp;&nbsp;UF&nbsp;&nbsp;
        </td>
        <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo number_format($_SESSION['reservaTotalF'],2,',','.');
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">

        </td>
        <td style="text-align: center; width: 44px; height: 10px; padding-left: 5px;">

        </td>
        <td style="text-align: right; width: 281px; height: 10px; padding-left: 11px;">
          Desc. 1 Aplicado&nbsp;&nbsp;&nbsp;&nbsp;%&nbsp;&nbsp;
        </td>
        <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo number_format($_SESSION['reservaDescuentoSala'],2,',','.');
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">

        </td>
        <td style="text-align: center; width: 44px; height: 10px; padding-left: 5px;">

        </td>
        <td style="text-align: right; width: 281px; height: 10px; padding-left: 11px;">
          Desc. 2 Aplicado&nbsp;&nbsp;&nbsp;&nbsp;%&nbsp;&nbsp;
        </td>
        <td style="text-align: center; width: 200px; border: 1px; height: 10px; padding-left: 5px;">
          <?php
            echo number_format($_SESSION['reservaDescuentoEspecial'],2,',','.');
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 12px;">
          <b>3. FORMA DE PAGO</b>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td style="width: 400px;">
          <table style="margin-top: 10px; margin-left: 42px;">
            <tr>
              <td style="text-align: left; width: 140px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; height: 10px;">
                <b>MONTO</b>
              </td>
              <td style="text-align: right; width: 22px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; height: 10px;">
                Porc. %
              </td>
            </tr>
          </table>
          <table style="margin-left: 40px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                CONTADO OFERTA:
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">
                UF
              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format($_SESSION['reservaReservaUF'],2,',','.');
                ?>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaReservaUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
          <table style="margin-left: 40px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                CONTADO PROMESA:
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">
                UF
              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format($_SESSION['reservaPiePromesaUF'],2,',','.');
                ?>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaPiePromesaUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
          <table style="margin-left: 40px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                CANTIDAD CUOTAS PIE:
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format($_SESSION['reservaCuotasPie'],2,',','.');
                ?>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">

              </td>
            </tr>
          </table>
          <table style="margin-left: 40px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                MONTO CUOTA PIE:
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">
                UF
              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  if ($_SESSION['reservaCuotasPie'] != '0') {
                    echo number_format($_SESSION['reservaPieSaldoUF']/$_SESSION['reservaCuotasPie'],2,',','.');
                  }
                  else{
                    echo '0,00';
                  }
                ?>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaPieSaldoUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
          <table style="margin-left: 40px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                TOTAL PIE:
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">
                UF
              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaPieSaldoUF']+$_SESSION['reservaPiePromesaUF']+$_SESSION['reservaReservaUF']),2,',','.');
                ?>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaPieSaldoUF']+$_SESSION['reservaPiePromesaUF']+$_SESSION['reservaReservaUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
          <table style="margin-left: 40px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                CREDITO HIPOTECARIO:
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">
                UF
              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaSaldoTotalUF']),2,',','.');
                ?>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px;">
                <?php
                  echo number_format(($_SESSION['reservaSaldoTotalUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
                ?>
              </td>
            </tr>
          </table>
          <table style="margin-left: 40px; margin-top: 10px;">
            <tr>
              <td style="text-align: left; width: 125px; height: 10px;">
                <b>TOTAL:</b>
              </td>
              <td style="text-align: left; width: 15px; height: 10px;">
                <b>UF</b>
              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px; background-color: #e3e3e3;">
                <b>
                <?php
                  echo number_format(($_SESSION['reservaTotalF']),2,',','.');
                ?>
                </b>
              </td>
              <td style="text-align: right; width: 20px; height: 10px;">

              </td>
              <td style="text-align: center; width: 50px; border: 1px; height: 10px; background-color: #e3e3e3;">
                <b>100,00</b>
              </td>
            </tr>
          </table>
        </td>
        <td style="width: 330px;">
          <table style="margin-top: 10px;">
            <tr>
              <td style="text-align: center; vertical-align: top;">
                <b>OBSERVACIONES</b>
              </td>
            </tr>
            <tr>
              <td style="height: 115px;border: 1px; width: 330px; text-align: justify; padding: 5px; vertical-align: top; font-size: 11px;">
                <?php
                  echo $_SESSION['observacionCrearReserva'];
                ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table style="margin-top: 45px; margin-left: 120px;">
      <tr>
        <td style="width: 150px; text-align: center;">
          <hr style="height: 1px;"/>
          Firma VB LIVING NET
        </td>
        <td style="width: 240px;">

        </td>
        <td style="width: 150px; text-align: center;">
          <hr style="height: 1px;"/>
          Firma VB
          <?php
            echo $_SESSION['reservaInmobiliaria'];
          ?>
        </td>
      </tr>
    </table>
  </body>
</html>
