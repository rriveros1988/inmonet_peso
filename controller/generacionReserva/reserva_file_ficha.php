<?php
//ini_set('display_errors', 'On');
require '../html2pdf/vendor/autoload.php';
date_default_timezone_set("America/Santiago");
session_start();

function pago($tasa, $monto, $meses){
  $I = $tasa / 12 / 100 ;
  $I2 = $I + 1 ;
  $I2 = pow($I2,-$meses) ;

  $CuotaMensual = ($I * $monto) / (1 - $I2);

  return $CuotaMensual;
}

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
/*
if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
  require_once 'cotizacion_plantilla_pdf_corretaje.php';
}
else{
  require_once 'cotizacion_plantilla_pdf.php';
}
*/
require_once 'reserva_plantilla_pdf_ficha.php';

$html = ob_get_clean();

// $document = '/var/www/html/Git/inmonet';
// $document = '/home/livingne/inmonet.cl/test';
$document = '/home/livingne/inmonet.cl';

if(!is_dir("../../repositorio/" . $_SESSION['reservaCodigoProyecto'])){
  mkdir("../../repositorio/" . $_SESSION['reservaCodigoProyecto'], 0777);
  mkdir("../../repositorio/" . $_SESSION['reservaCodigoProyecto'] . "/reserva", 0777);
}
if(!is_dir("../../repositorio/" . $_SESSION['reservaCodigoProyecto'] . "/reserva")){
  mkdir("../../repositorio/" . $_SESSION['reservaCodigoProyecto'] . "/reserva", 0777);
}

$html2pdf = new Html2Pdf('P','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
$html2pdf->output($document . '/repositorio/' . $_SESSION['reservaCodigoProyecto'] . '/reserva/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['reservaCodigoProyecto'] . '_' . $_SESSION['reservaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['reservaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['reservaApellidoCliente']) . '_FC.pdf', 'F');

$_SESSION['FICHA_PDF_ACTUAL'] = '';

if(file_exists($document . '/repositorio/' . $_SESSION['reservaCodigoProyecto'] . '/reserva/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['reservaCodigoProyecto'] . '_' . $_SESSION['reservaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['reservaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['reservaApellidoCliente']) . '_FC.pdf')){
  echo "Ok";
  actualizaFICHA_PDF($_SESSION['reservaCodigoProyecto'] . '/reserva/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['reservaCodigoProyecto'] . '_' . $_SESSION['reservaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['reservaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['reservaApellidoCliente']) . '_FC.pdf',$_SESSION['idReserva']);
  $_SESSION['FICHA_PDF_ACTUAL'] = $_SESSION['reservaCodigoProyecto'] . '/reserva/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['reservaCodigoProyecto'] . '_' . $_SESSION['reservaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['reservaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['reservaApellidoCliente']) . '_FC.pdf';
}
else{
  echo "Sin datos";
}
?>
