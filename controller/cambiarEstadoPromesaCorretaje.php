<?php
	// ini_set('display_errors', 'On');
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();
	if(count($_POST) > 0){

		$duenoFormaPago = $_POST['duenoFormaPago'];
		$duenoBanco = $_POST['duenoBanco'];
		$duenoSerie = $_POST['duenoSerie'];
		$duenoNro = $_POST['duenoNro'];
		$duenoFechaCheque = $_POST['duenoFechaCheque'];
		if($duenoFechaCheque == ""){
			$duenoFechaCheque = NULL;
		}
		$clienteFormaPago = $_POST['clienteFormaPago'];
		$clienteBanco = $_POST['clienteBanco'];
		$clienteSerie = $_POST['clienteSerie'];
		$clienteNro = $_POST['clienteNro'];
		$clienteFechaCheque = $_POST['clienteFechaCheque'];
		if($clienteFechaCheque == ""){
			$clienteFechaCheque = NULL;
		}
		$codigoProyecto = $_POST['comision_codigoProyecto'];
		$numeroOperacion = $_POST['comision_numeroOperacion'];
		$estadoPromesa = $_POST['comision_estadoPromesa'];
		$_SESSION['estadoPromesa'] = $estadoPromesa;
		$unidadRevisar = $_POST['comision_unidadRevisar'];
		$omitir = $_POST['omitir'];
		$proy = consultaProyectoEspecificoCodigo($codigoProyecto);
			// var_dump($unidadRevisar);
		$row = cambiarEstadoPromesaCorretaje($codigoProyecto, $numeroOperacion, $estadoPromesa);

		$comCorretaje = consultaComisionPromesaCorretaje($codigoProyecto, $numeroOperacion, $unidadRevisar)[0];
		$totalUnidad = $comCorretaje['VALORUF'];
		$accion = $comCorretaje['IDACCION'];
		$promesa = datosPromesaParaComision($codigoProyecto, $numeroOperacion);

		$idUsuario = $promesa[0]["IDUSUARIO"];
		$fechaComisionPromesa = new DateTime($promesa[0]['FECHAPROMESA']);
		$fechaComisionPromesa = $fechaComisionPromesa->format('Y-m-d');
		$idProyecto = $proy[0]["IDPROYECTO"];
		$idPromesa = $promesa[0]["IDPROMESA"];
		$_SESSION['idPromesa'] = $promesa[0]["IDPROMESA"];

		//Datos de valores comisiones corretaje
		$valorUFComDueno = $_POST['valorUFComDueno'];
    $montoPagoComDueno = $_POST['montoPagoComDueno'];
		$valorUFComCliente = $_POST['valorUFComCliente'];
		$montoPagoComCliente = $_POST['montoPagoComCliente'];

		if($valorUFComDueno == ''){
			$valorUFComDueno = 0;
		}
		if($montoPagoComDueno == ''){
			$montoPagoComDueno= 0;
		}
		if($valorUFComCliente == ''){
			$valorUFComCliente = 0;
		}
		if($montoPagoComCliente == ''){
			$montoPagoComCliente = 0;
		}

		if ($row->error == "") {
			if ($estadoPromesa == '2') {
				$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
					    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
					    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
					    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
					    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
						$proyecto = consultaProyectoEspecifico($idProyecto);
				        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
				        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
				        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
				        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
						$fechaPromesa = $fecha->format("d-m-Y");
						$_SESSION['promesaFecha'] = $fechaPromesa;
						$_SESSION['numeroOperacion'] = $numeroOperacion;
						$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
				        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
				        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
				        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
				        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
				        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
				        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
				        if ($cliente1[0]['TIPODOMICILIO'] != '') {
					        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
				        }else{
							$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
				        }
				        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
				        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
				        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
				        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
				        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
				        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
				        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
				        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
				        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
				        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
				        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
				        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
				        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
				        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
				        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
				        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
				        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
				        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
				        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
				        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
				        if(count($cuotas_promesa) > 0){
					        $_SESSION['promesaFormaPagoCuota'] = array();

					        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

					        foreach ($cuotasPromesa as $cuota) {

					            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));

					        }
					      }
					      else{
					        unset($_SESSION['cuotasPromesa']);
					      }
				        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
				        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
				        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
					    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
					    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
					    $_SESSION['MONTOUFCOMISION'] = '&nbsp;&nbsp;';
						$_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						$_SESSION['FORMAPAGOCOMISION'] = consultaFormaPagoReservaEspecifica($clienteFormaPago)[0]['NOMBRE'];
						$_SESSION['BANCOCOMISION'] = $clienteBanco;
						$_SESSION['SERIECOMISION'] = $clienteSerie;
						$_SESSION['NROCOMISION'] = $clienteNro;
						if(is_null($clienteFechaCheque)){
							$fechaPagoComision = NULL;
						}
						else{
							$fechaPagoComision = new DateTime($clienteFechaCheque);
							$fechaMostrar= $fechaPagoComision->format('d-m-Y');
						}
						$_SESSION['FECHAPAGOCOMISION'] = $fechaMostrar;
						eliminaFormaPagoComision($codigoProyecto, $numeroOperacion, $row);
						eliminaComisionPromesaCorretaje($codigoProyecto, $numeroOperacion, $row);
						$row->query("COMMIT");
			}
			if (count(checkComisionPromesa($codigoProyecto, $numeroOperacion, $idUsuario)) < 2) {
		    	if($estadoPromesa == 3){
		    		if ($accion == 1){ //Venta
						if ($comCorretaje['COMISIONLVDUENO'] != '0' && $comCorretaje['COMISIONLVCLIENTE'] != '0') {
							$comVentaDueno = $comCorretaje['COMISIONLVDUENO'];
							$comVentaCliente = $comCorretaje['COMISIONLVCLIENTE'];

							$montoVentaDueno = ($comVentaDueno / 100) * $totalUnidad;
							$montoVentaCliente = ($comVentaCliente / 100) * $totalUnidad;

							$insercionDueno = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoVentaDueno, $comVentaDueno, 'Dueño',$row);
							$idPagoDueno = $insercionDueno->insert_id;

							if ($insercionDueno->error == "") {
								if ($omitir == false) {
									if ($duenoFormaPago != "" && $duenoBanco != "" && $duenoSerie != "" && $duenoNro != "" && $duenoFechaCheque != "" && $clienteFormaPago != "" && $clienteBanco != "" && $clienteSerie != "" && $clienteNro != "" && $clienteFechaCheque != "" ) {
										//Procesamiento de fecha
										if(is_null($duenoFechaCheque)){
											$fechaComisionDueno = NULL;
										}
										else{
											$fechaComisionDueno = new DateTime($duenoFechaCheque);
											$fechaComisionDueno = $fechaComisionDueno->format('Y-m-d');
										}

										$idPagoDueno = $insercionDueno->insert_id;

										$rowD = ingresoFormaPagoComision($idPagoDueno, $duenoFormaPago, $duenoBanco, $duenoSerie, $duenoNro, $fechaComisionDueno, $row, $valorUFComDueno, $montoPagoComDueno);

										if ($rowD->error == "") {
											$row->query("COMMIT");
											$insercionDueno->query("COMMIT");
											$rowD->query("COMMIT");
											$insercionCliente = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoVentaCliente, $comVentaCliente, 'Cliente',$row);
											$idPagoCliente = $insercionCliente->insert_id;
											if(is_null($clienteFechaCheque)){
												$fechaComisionCliente = NULL;
											}
											else{
												$fechaComisionCliente = new DateTime($clienteFechaCheque);
												$fechaComisionCliente = $fechaComisionCliente->format('Y-m-d');
											}
											$rowC = ingresoFormaPagoComision($idPagoCliente, $clienteFormaPago, $clienteBanco, $clienteSerie, $clienteNro, $fechaComisionCliente, $row, $valorUFComCliente, $montoPagoComCliente);
											$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
										    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
										    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
										    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
										    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
											$proyecto = consultaProyectoEspecifico($idProyecto);
									        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
									        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
									        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
									        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
											$fechaPromesa = $fecha->format("d-m-Y");
											$_SESSION['promesaFecha'] = $fechaPromesa;
											$_SESSION['numeroOperacion'] = $numeroOperacion;
											$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
									        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
									        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
									        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
									        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
									        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
									        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
									        if ($cliente1[0]['TIPODOMICILIO'] != '') {
										        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
									        }else{
												$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
									        }
									        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
									        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
									        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
									        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
									        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
									        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
									        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
									        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
									        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
									        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
									        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
									        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
									        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
									        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
									        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
									        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
									        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
									        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
									        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
									        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
									        if(count($cuotas_promesa) > 0){
										        $_SESSION['promesaFormaPagoCuota'] = array();

										        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

										        foreach ($cuotasPromesa as $cuota) {

										            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));

										        }
										      }
										      else{
										        unset($_SESSION['cuotasPromesa']);
										      }
									        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
									        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
									        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
										    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
										    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
										    $_SESSION['MONTOUFCOMISION'] = number_format($montoVentaCliente, 2, ',', '.');
											$_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['FORMAPAGOCOMISION'] = consultaFormaPagoReservaEspecifica($clienteFormaPago)[0]['NOMBRE'];
											$_SESSION['BANCOCOMISION'] = $clienteBanco;
											$_SESSION['SERIECOMISION'] = $clienteSerie;
											$_SESSION['NROCOMISION'] = $clienteNro;
											if(is_null($clienteFechaCheque)){
												$fechaPagoComision = NULL;
											}
											else{
												$fechaPagoComision = new DateTime($clienteFechaCheque);
												$fechaMostrar= $fechaPagoComision->format('d-m-Y');
											}
											$_SESSION['FECHAPAGOCOMISION'] = $fechaMostrar;

											$row->query("COMMIT");
											$insercionCliente->query("COMMIT");
											$rowC->query("COMMIT");
											echo "Ok";
										}else{
											$row->query("ROLLBACK");
											$insercionDueno->query("ROLLBACK");
											$insercionCliente->query("ROLLBACK");
											$rowD->query("ROLLBACK");
											$rowC->query("ROLLBACK");
											echo "Sin datos";
										}
									}else{
										$row->query("ROLLBACK");
										$insercionDueno->query("ROLLBACK");
										if (isset($insercionCliente)) {
											$insercionCliente->query("ROLLBACK");
										}
										echo "faltandatos";
									}
								}else{
									//Procesamiento de fecha
									if(is_null($duenoFechaCheque)){
										$fechaComisionDueno = NULL;
									}
									else{
										$fechaComisionDueno = new DateTime($duenoFechaCheque);
										$fechaComisionDueno = $fechaComisionDueno->format('Y-m-d');
									}

									$idPagoDueno = $insercionDueno->insert_id;

									$rowD = ingresoFormaPagoComision($idPagoDueno, $duenoFormaPago, $duenoBanco, $duenoSerie, $duenoNro, $fechaComisionDueno, $row, $valorUFComDueno, $montoPagoComDueno);

									if ($rowD->error == "") {
										$row->query("COMMIT");
										$insercionDueno->query("COMMIT");
										$rowD->query("COMMIT");
										$insercionCliente = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoVentaCliente, $comVentaCliente, 'Cliente',$row);
										$idPagoCliente = $insercionCliente->insert_id;

										if(is_null($clienteFechaCheque)){
											$fechaComisionCliente = NULL;
										}
										else{
											$fechaComisionCliente = new DateTime($clienteFechaCheque);
											$fechaComisionCliente = $fechaComisionCliente->format('Y-m-d');
										}
										$rowC = ingresoFormaPagoComision($idPagoCliente, $clienteFormaPago, $clienteBanco, $clienteSerie, $clienteNro, $fechaComisionCliente, $row, $valorUFComCliente, $montoPagoComCliente);
										$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
										    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
										    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
										    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
										    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
											$proyecto = consultaProyectoEspecifico($idProyecto);
									        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
									        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
									        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
									        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
											$fechaPromesa = $fecha->format("d-m-Y");
											$_SESSION['promesaFecha'] = $fechaPromesa;
											$_SESSION['numeroOperacion'] = $numeroOperacion;
											$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
									        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
									        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
									        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
									        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
									        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
									        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
									        if ($cliente1[0]['TIPODOMICILIO'] != '') {
										        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
									        }else{
												$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
									        }
									        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
									        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
									        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
									        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
									        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
									        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
									        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
									        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
									        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
									        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
									        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
									        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
									        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
									        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
									        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
									        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
									        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
									        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
									        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
									        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
									        if(count($cuotas_promesa) > 0){
										        $_SESSION['promesaFormaPagoCuota'] = array();

										        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

										        foreach ($cuotasPromesa as $cuota) {

										            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));

										        }
										      }
										      else{
										        unset($_SESSION['cuotasPromesa']);
										      }
									        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
									        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
									        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
										    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
										    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
										    $_SESSION['MONTOUFCOMISION'] = number_format($montoVentaCliente, 2, ',', '.');
											$_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['FORMAPAGOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['BANCOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['SERIECOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['NROCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['FECHAPAGOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
										$insercionCliente->query("COMMIT");
										$rowC->query("COMMIT");
										echo "Ok";
									}else{
										$row->query("ROLLBACK");
										$insercionDueno->query("ROLLBACK");
										$insercionCliente->query("ROLLBACK");
										$rowD->query("ROLLBACK");
										$rowC->query("ROLLBACK");
										echo "Sin datos";
									}
								}
							}else{
								echo "Sin datos";
							}
						}else{
							echo "sincomisiones";
						}
					}else{ // Arriendo

						if ($comCorretaje['COMISIONVENDUENO'] != '0' && $comCorretaje['COMISIONVENCLIENTE'] != '0') {
							$comArriendoDueno = $comCorretaje['COMISIONVENDUENO'];
							$comArriendoCliente = $comCorretaje['COMISIONVENCLIENTE'];

							$montoArriendoDueno = ($comArriendoDueno / 100) * $totalUnidad;
							$montoArriendoCliente = ($comArriendoCliente / 100) * $totalUnidad;

							$insercionDueno = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoArriendoDueno, $comArriendoDueno, 'Dueño',$row);
							$idPagoDueno = $insercionDueno->insert_id;

							if ($insercionDueno->error == "") {
								if ($omitir == false) {
									if ($duenoFormaPago != "" && $duenoBanco != "" && $duenoSerie != "" && $duenoNro != "" && $duenoFechaCheque != "" && $clienteFormaPago != "" && $clienteBanco != "" && $clienteSerie != "" && $clienteNro != "" && $clienteFechaCheque != "" ) {
										//Procesamiento de fecha
										if(is_null($duenoFechaCheque)){
											$fechaComisionDueno = NULL;
										}
										else{
											$fechaComisionDueno = new DateTime($duenoFechaCheque);
											$fechaComisionDueno = $fechaComisionDueno->format('Y-m-d');
										}

										$idPagoDueno = $insercionDueno->insert_id;

										$rowD = ingresoFormaPagoComision($idPagoDueno, $duenoFormaPago, $duenoBanco, $duenoSerie, $duenoNro, $fechaComisionDueno, $row, $valorUFComDueno, $montoPagoComDueno);

										if ($rowD->error == "") {
											$row->query("COMMIT");
											$insercionDueno->query("COMMIT");
											$rowD->query("COMMIT");
											$insercionCliente = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoArriendoCliente, $comArriendoCliente, 'Cliente',$row);
											$idPagoCliente = $insercionCliente->insert_id;
											if(is_null($clienteFechaCheque)){
												$fechaComisionCliente = NULL;
											}
											else{
												$fechaComisionCliente = new DateTime($clienteFechaCheque);
												$fechaComisionCliente = $fechaComisionCliente->format('Y-m-d');
											}
											$rowC = ingresoFormaPagoComision($idPagoCliente, $clienteFormaPago, $clienteBanco, $clienteSerie, $clienteNro, $fechaComisionCliente, $row, $valorUFComCliente, $montoPagoComCliente);
											$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
										    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
										    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
										    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
										    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
											$proyecto = consultaProyectoEspecifico($idProyecto);
									        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
									        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
									        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
									        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
											$fechaPromesa = $fecha->format("d-m-Y");
											$_SESSION['promesaFecha'] = $fechaPromesa;
											$_SESSION['numeroOperacion'] = $numeroOperacion;
											$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
									        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
									        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
									        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
									        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
									        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
									        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
									        if ($cliente1[0]['TIPODOMICILIO'] != '') {
										        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
									        }else{
												$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
									        }
									        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
									        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
									        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
									        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
									        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
									        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
									        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
									        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
									        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
									        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
									        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
									        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
									        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
									        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
									        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
									        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
									        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
									        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
									        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
									        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
									        if(count($cuotas_promesa) > 0){
										        $_SESSION['promesaFormaPagoCuota'] = array();

										        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

										        foreach ($cuotasPromesa as $cuota) {

										            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));

										        }
										      }
										      else{
										        unset($_SESSION['cuotasPromesa']);
										      }
									        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
									        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
									        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
										    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
										    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
										    $_SESSION['MONTOUFCOMISION'] = number_format($montoArriendoCliente, 2, ',', '.');
											$_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['FORMAPAGOCOMISION'] = consultaFormaPagoReservaEspecifica($clienteFormaPago)[0]['NOMBRE'];
											$_SESSION['BANCOCOMISION'] = $clienteBanco;
											$_SESSION['SERIECOMISION'] = $clienteSerie;
											$_SESSION['NROCOMISION'] = $clienteNro;
											if(is_null($clienteFechaCheque)){
												$fechaPagoComision = NULL;
											}
											else{
												$fechaPagoComision = new DateTime($clienteFechaCheque);
												$fechaMostrar= $fechaPagoComision->format('d-m-Y');
											}
											$_SESSION['FECHAPAGOCOMISION'] = $fechaMostrar;

											$row->query("COMMIT");
											$insercionCliente->query("COMMIT");
											$rowC->query("COMMIT");
											echo "Ok";
										}else{
											$row->query("ROLLBACK");
											$insercionDueno->query("ROLLBACK");
											$insercionCliente->query("ROLLBACK");
											$rowD->query("ROLLBACK");
											$rowC->query("ROLLBACK");
											echo "Sin datos";
										}
									}else{
										$row->query("ROLLBACK");
										$insercionDueno->query("ROLLBACK");
										if (isset($insercionCliente)) {
											$insercionCliente->query("ROLLBACK");
										}
										echo "faltandatos";
									}
								}else{
									//Procesamiento de fecha
									if(is_null($duenoFechaCheque)){
										$fechaComisionDueno = NULL;
									}
									else{
										$fechaComisionDueno = new DateTime($duenoFechaCheque);
										$fechaComisionDueno = $fechaComisionDueno->format('Y-m-d');
									}

									$idPagoDueno = $insercionDueno->insert_id;

									$rowD = ingresoFormaPagoComision($idPagoDueno, $duenoFormaPago, $duenoBanco, $duenoSerie, $duenoNro, $fechaComisionDueno, $row, $valorUFComDueno, $montoPagoComDueno);

									if ($rowD->error == "") {
										$row->query("COMMIT");
										$insercionDueno->query("COMMIT");
										$rowD->query("COMMIT");
										$insercionCliente = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoArriendoCliente, $comArriendoCliente, 'Cliente',$row);
										$idPagoCliente = $insercionCliente->insert_id;
										if(is_null($clienteFechaCheque)){
											$fechaComisionCliente = NULL;
										}
										else{
											$fechaComisionCliente = new DateTime($clienteFechaCheque);
											$fechaComisionCliente = $fechaComisionCliente->format('Y-m-d');
										}
										$rowC = ingresoFormaPagoComision($idPagoCliente, $clienteFormaPago, $clienteBanco, $clienteSerie, $clienteNro, $fechaComisionCliente, $row, $valorUFComCliente, $montoPagoComCliente);
										$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
										    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
										    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
										    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
										    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
											$proyecto = consultaProyectoEspecifico($idProyecto);
									        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
									        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
									        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
									        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
											$fechaPromesa = $fecha->format("d-m-Y");
											$_SESSION['promesaFecha'] = $fechaPromesa;
											$_SESSION['numeroOperacion'] = $numeroOperacion;
											$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
									        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
									        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
									        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
									        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
									        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
									        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
									        if ($cliente1[0]['TIPODOMICILIO'] != '') {
										        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
									        }else{
												$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
									        }
									        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
									        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
									        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
									        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
									        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
									        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
									        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
									        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
									        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
									        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
									        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
									        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
									        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
									        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
									        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
									        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
									        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
									        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
									        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
									        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
									        if(count($cuotas_promesa) > 0){
										        $_SESSION['promesaFormaPagoCuota'] = array();

										        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

										        foreach ($cuotasPromesa as $cuota) {

										            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));

										        }
										      }
										      else{
										        unset($_SESSION['cuotasPromesa']);
										      }
									        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
									        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
									        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
										    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
										    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
										    $_SESSION['MONTOUFCOMISION'] = number_format($montoArriendoCliente, 2, ',', '.');
											$_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
											$_SESSION['FORMAPAGOCOMISION'] = consultaFormaPagoReservaEspecifica($clienteFormaPago)[0]['NOMBRE'];
											$_SESSION['BANCOCOMISION'] = $clienteBanco;
											$_SESSION['SERIECOMISION'] = $clienteSerie;
											$_SESSION['NROCOMISION'] = $clienteNro;
											if(is_null($clienteFechaCheque)){
												$fechaPagoComision = NULL;
											}
											else{
												$fechaPagoComision = new DateTime($clienteFechaCheque);
												$fechaMostrar= $fechaPagoComision->format('d-m-Y');
											}
											$_SESSION['FECHAPAGOCOMISION'] = $fechaMostrar;
										$insercionCliente->query("COMMIT");
										$rowC->query("COMMIT");
										echo "Ok";
									}else{
										$row->query("ROLLBACK");
										$insercionDueno->query("ROLLBACK");
										$insercionCliente->query("ROLLBACK");
										$rowD->query("ROLLBACK");
										$rowC->query("ROLLBACK");
										echo "Sin datos";
									}
								}
							}else{
								echo "Sin datos";
							}
						}else{
							echo "sincomisiones";
						}
					}
	    		}else{
	    			echo "Sin datos";
	    		}
			}else{
				echo "Existente";
			}
		}else{
			echo "Sin datos";
		}
	}else{
		echo "Sin datos";
	}
?>
