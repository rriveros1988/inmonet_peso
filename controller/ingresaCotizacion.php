<?php
  header('Access-Control-Allow-Origin: *');
  // ini_set('display_errors', 'On');
  require('../model/consultas.php');
  session_start();

	if(count($_SESSION) > 0 && count($_POST) > 0){
    $datosUnidadArray = datosUnidad($_SESSION['codProyectoClienteCotizacion'],$_SESSION['departamentoClienteCotizacion'],$_SESSION['accionClienteCotizacion']);
    $idClienteArray = chequeaCliente($_SESSION['rutClienteCotizacion']);
    $idUsuarioArray = chequeaUsuario($_SESSION['rutUser']);
    $datosUsuarioArray = consultaDatosUsuario($_SESSION['rutUser']);

    $_SESSION['fonoUsuario'] = $datosUsuarioArray[0]['FONO'];
    $_SESSION['mailUsuario'] = $datosUsuarioArray[0]['EMAIL'];

    $idProyecto = $datosUnidadArray['IDPROYECTO'];
    $idUnidad = $datosUnidadArray['IDUNIDAD'];
    $idCliente = $idClienteArray['IDCLIENTE'];
    $idUsuario = $idUsuarioArray['IDUSUARIO'];
    $codigo = 0;
    $fecha = new DateTime($_SESSION['fechaCotizacion']);
    $fecha = $fecha->format("Y-m-d");
    $_SESSION['valorUFJqueryHoy'] = $_POST['valorUFJqueryHoy'];
    $valorTotalUF  = $_SESSION['totalFUFClienteCotizacion'];
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      if($_SESSION['accionClienteCotizacion'] == 'Venta'){
        $descuento1UF = 0;
        $descuento2UF = 0;
        $valorReservaUF = 0;
        $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
        $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
        $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
        $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
        $bonoVenta = 0;
        $_SESSION['datosUnidad'] = consultaUnidadEspecifica($idUnidad);
        $_SESSION['datosProyecto'] = consultaDatosProyecto($_SESSION['codProyectoClienteCotizacion']);
      }
      else{
        $descuento1UF = 0;
        $descuento2UF = 0;
        $valorReservaUF = 0;
        $valorPiePromesaUF = 0;
        $valorPieSaldoUF = 0;
        $cantidadCuotasPie = 0;
        $valorSaldoTotalUF = 0;
        $bonoVenta = 0;
        $_SESSION['datosUnidad'] = consultaUnidadEspecifica($idUnidad);
        $_SESSION['datosProyecto'] = consultaDatosProyecto($_SESSION['codProyectoClienteCotizacion']);
      }
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorReservaUF = $_SESSION['reservaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }

    if($_SESSION['bodegasClienteCotizacion'] == ''){
      $_SESSION['bodegasClienteCotizacion'] = 0;
    }
    if($_SESSION['estacionamientosClienteCotizacion'] == ''){
      $_SESSION['estacionamientosClienteCotizacion'] = 0;
    }

    $medio = $_SESSION['publicidadClienteCotizacion'];
    $cotizaEn = $_SESSION['visitaClienteCotizacion'];


    $row = ingresaCotizacion($idProyecto, $idUnidad, $idCliente, $idUsuario,$codigo,$fecha,$valorTotalUF,$descuento1UF,$descuento2UF,$valorReservaUF,$valorPiePromesaUF,$valorPieSaldoUF,$cantidadCuotasPie,$valorSaldoTotalUF,$medio,$cotizaEn, $bonoVenta, $_SESSION['bodegasClienteCotizacion'], $_SESSION['estacionamientosClienteCotizacion'], $_SESSION['departamentoUfClienteCotizacion']);

    if($row != "Error")
    {
      $_SESSION['idCotizacion'] = $row->insert_id;

      $in = 'Ok';
      $in2 = 'Ok';

      if(is_array($_POST['bodegasClienteCotizacion'])){
        for($i = 0; $i < count($_POST['bodegasClienteCotizacion']); $i++){
          $in = ingresaCotizacionBodega($_SESSION['idCotizacion'],$_POST['bodegasClienteCotizacion'][$i], $row);
          if($in == "Error"){
            break;
          }
        }
      }

      if(is_array($_POST['estacionamientosClienteCotizacion'])){
        for($i = 0; $i < count($_POST['estacionamientosClienteCotizacion']); $i++){
          $in2 = ingresaCotizacionEstacionamiento($_SESSION['idCotizacion'],$_POST['estacionamientosClienteCotizacion'][$i], $row);
          if($in2 == "Error"){
            break;
          }
        }
      }

      if($in != "Error" && $in2 != "Error"){
        $row->query("COMMIT");
        $numCot = numeroCotizacion($_SESSION['idCotizacion']);
        $_SESSION['numeroCotizacion'] = $numCot['CODIGO'];

        echo "Ok¬" . $_SESSION['codProyectoClienteCotizacion'] . "¬" . $_SESSION['numeroCotizacion'];
      }
      else{
        echo "Sin datos";
      }
    }
    else{
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>
