<?php
// ini_set('display_errors', 'On');
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');

	if(count($_POST) > 0){
    $codigoProyecto = $_POST['escritura_codigoProyecto'];
    $numeroOperacion = $_POST['escritura_numeroOperacion'];

    $consultaEscritura = consultaEscrituraEspecifica($codigoProyecto,$numeroOperacion);

    $idEscritura = $consultaEscritura[0]['IDESCRITURA'];
    $idUnidad = $consultaEscritura[0]['IDUNIDAD'];
    //Cambio de estado a Unidad de la Escritura
    cambioEstadoUnidadesEscritura($idUnidad);

    liberaUnidadesEscrituraEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesEscrituraBodega($codigoProyecto,$numeroOperacion);
    liberaUnidadesEscrituradas($codigoProyecto,$numeroOperacion);
    eliminaEscrituraBodega($codigoProyecto, $numeroOperacion);
    eliminaEscrituraEstacionamiento($codigoProyecto, $numeroOperacion);

    eliminaEscrituraCuota($codigoProyecto,$numeroOperacion);

    eliminaComentarioEscritura($codigoProyecto, $idUnidad, $numeroOperacion);
    eliminaComisionEscritura($codigoProyecto, $numeroOperacion);
    //Borrar documentos
    // $document = 'D:/MAMP/htdocs/inmonet';
    // $document = '/var/www/html/Git/inmonet';
    $document = '/home/livingne/inmonet.cl';

    $row = eliminaEscritura($codigoProyecto,$numeroOperacion);

    var_dump($row);

    if($row == "Ok")
    {
        $rutaES = $document . '/repositorio/' . $consultaEscritura[0]['ES_PDF'];
        if (file_exists($rutaES))
        {
        unlink($rutaES);
        }

        $rutaEN = $document . '/repositorio/' . $consultaEscritura[0]['EN_PDF'];
        if (file_exists($rutaEN))
        {
        unlink($rutaEN);
        }

        $rutaDE = $document . '/repositorio/' . $consultaEscritura[0]['DE_PDF'];
        if (file_exists($rutaDE))
        {
        unlink($rutaDE);
        }

        if($consultaEscritura[0]['ESTADO'] != '1' && $consultaEscritura[0]['ESTADO'] != '8'){
          //Cambia estado de promesa a promesada
          cambiarEstadoPromesa($codigoProyecto, $numeroOperacion, 3);
          //Cambia estado de reserva a promesada
          cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 6);
        }

        echo "Ok";
    }
    else{
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos por post";
	}
?>
