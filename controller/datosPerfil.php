<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');

	if(count($_POST) == 0){
    	$row = consultaPerfil();

        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){
                      $return = $return . utf8_encode($row[$i]['IDPERFIL']) . ',' . utf8_encode($row[$i]['NOMBREPERFIL']);
                    }
                    else{
                      $return = $return . ',' . utf8_encode($row[$i]['IDPERFIL']) . ',' . utf8_encode($row[$i]['NOMBREPERFIL']);
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>
