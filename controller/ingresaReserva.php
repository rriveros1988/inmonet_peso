<?php
  header('Access-Control-Allow-Origin: *');
  // ini_set('display_errors', 'On');
  require('../model/consultas.php');
	require("phpmailer/PHPMailerAutoload.php");
  session_start();

if(count($_SESSION) > 0 && count($_POST) > 0){
    $idCotizacion = $_SESSION['idCotizacion'];
    $idProyecto = $_SESSION['idProyecto'];
    $idUnidad = $_SESSION['idUnidad'];
    $idCliente1 = $_SESSION['idCliente1'];
    $idCliente2 = 0; //Pendiente
    $_SESSION['idUsuario'] = $_POST['idUsuario'];
    $idUsuario = $_SESSION['idUsuario'];
    $fecha = new DateTime();
    $fechaReserva = $fecha->format("Y-m-d");
    $valorBrutoUF = $_SESSION['reservaValorBrutoUF'];
    $descuento1 = $_SESSION['reservaDescuentoSala'];
    $descuento2 = $_SESSION['reservaDescuentoEspecial'];
    $bonoVentaUF = $_SESSION['reservaBono'];
    $valorTotalUF = $_SESSION['reservaTotalF'];
    $valorReservaUF = $_POST['valorReservaUF'];
    $valorPiePromesaUF = $_POST['valorPiePromesaUF'];
    $valorPieSaldoUF = $_POST['valorPieSaldoUF'];
    $cantidadCuotasPie = $_POST['cantidadCuotasPie'];
    $valorSaldoTotalUF = $_POST['valorSaldoTotalUF'];
    $fecha2 =  new DateTime($_POST['fechaPagoReserva']);
    $fechaPagoReserva = $fecha2->format("Y-m-d");
    $valorPagoReserva = $_POST['valorPagoReserva'];
    $formaPagoReserva = $_POST['formaPagoReserva'];
    $bancoReserva = $_POST['bancoReserva'];
    $serieChequeReserva = $_POST['serieChequeReserva'];
    $nroChequeReserva = $_POST['nroChequeReserva'];
    $valorPiePromesa = $_POST['valorPiePromesa'];
    $valorPieSaldo = $_POST['valorPieSaldo'];
    $observacionCrearReserva = $_POST['observacionCrearReserva'];

    if(array_key_exists('reservaIdVendedor', $_SESSION)){
      $vendedor = chequeaUsuarioID($_SESSION['reservaIdVendedor']);
      $_SESSION['reservaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
      $_SESSION['reservaVendedorRut'] = $vendedor['RUT'];
    }
    else{
      $_SESSION['reservaVendedor'] = $_SESSION['nombreUser'];
      $_SESSION['reservaVendedorRut'] = $_SESSION['rutUser'];
    }

    /*
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      $descuento1UF = 0;
      $descuento2UF = 0;
      $valorReservaUF = 0;
      $valorPiePromesaUF = 0;
      $valorPieSaldoUF = 0;
      $cantidadCuotasPie = 0;
      $valorSaldoTotalUF = 0;
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorReservaUF = $_SESSION['reservaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }
    */

    $cotizaDatos = consultaCotizacionEspecificaID($idCotizacion);
    $cotizaPDF = $cotizaDatos[0]['RUTA_PDF'];

    $row = ingresaReserva($idCotizacion, $idProyecto, $idUnidad, $idCliente1, $idCliente2, $idUsuario, $fechaReserva, $valorBrutoUF, $descuento1, $descuento2, $bonoVentaUF, $valorTotalUF, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoReserva, $valorPagoReserva, $formaPagoReserva, $bancoReserva, $serieChequeReserva, $nroChequeReserva, $valorPiePromesa, $valorPieSaldo, $cotizaPDF);

    if($row != "Error")
    {
      $_SESSION['idReserva'] = $row->insert_id;

      actualizaEstadoUnidad($idUnidad, 3, $row);
      actualizaInteresCotizacion($idCotizacion, $row);

      $in = 'Ok';
      $in2 = 'Ok';

      if($_POST['bodegasClienteReserva'] != ''){
        for($i = 0; $i < count($_POST['bodegasClienteReserva']); $i++){
          $in = ingresaReservaBodega($_SESSION['idReserva'],$_POST['bodegasClienteReserva'][$i], $row);
          if($in == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['bodegasClienteReserva'][$i], 3, $row);
          }
        }
      }

      if(is_array($_POST['estacionamientosClienteReserva'])){
        for($i = 0; $i < count($_POST['estacionamientosClienteReserva']); $i++){
          $in2 = ingresaReservaEstacionamiento($_SESSION['idReserva'],$_POST['estacionamientosClienteReserva'][$i], $row);
          if($in2 == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['estacionamientosClienteReserva'][$i], 3, $row);
          }
        }
      }

      if($in != "Error" && $in2 != "Error"){
        $row->query("COMMIT");
        $numCot = numeroOperacion($_SESSION['idReserva']);
        $_SESSION['numeroOperacion'] = $numCot['NUMERO'];

        //Datos a session
        $fechaReserva = $fecha->format("d-m-Y");
        $fechaPagoReserva = $fecha2->format("d-m-Y");
        $_SESSION['reservaFecha'] = $fechaReserva;
        $_SESSION['reservaReservaUF'] = $valorReservaUF;
        $_SESSION['reservaPiePromesaUF'] = $valorPiePromesaUF;
        $_SESSION['reservaPieSaldoUF'] = $valorPieSaldoUF;
        $_SESSION['reservaCuotasPie'] = $cantidadCuotasPie;
        $_SESSION['reservaSaldoTotalUF'] = $valorSaldoTotalUF;
        $_SESSION['reservaFechaPagoReserva'] = $fechaPagoReserva;
        $_SESSION['reservaValorPagoReserva'] = $valorPagoReserva;
        $_SESSION['reservaFormaPagoReserva'] = $formaPagoReserva;
        $_SESSION['reservaBancoReserva'] = $bancoReserva;
        $_SESSION['reservaSerieChequeReserva'] = $serieChequeReserva;
        $_SESSION['reservaNroChequeReserva'] = $nroChequeReserva;
        $_SESSION['reservaValorPiePromesa'] = $valorPiePromesa;
        $_SESSION['reservaValorPieSaldo'] = $valorPieSaldo;

        $cliente1 = consultaClienteEspecifico($_SESSION['idCliente1']);

        $fechaC1 = new DateTime($cliente1[0]['FECHANAC']);
        $fechaNacCliente1 = $fechaC1->format("d-m-Y");

        $_SESSION['reservaFechaNacCliente1'] = $fechaNacCliente1;
        $_SESSION['reservaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
        $_SESSION['reservaProfesionCliente1'] = $cliente1[0]['PROFESION'];
        $_SESSION['reservaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ' ' . $cliente1[0]['TIPODOMICILIO'];
        $_SESSION['reservaComunaCliente1'] = $cliente1[0]['COMUNA'];
        $_SESSION['reservaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
        $_SESSION['reservaMailCliente1'] = $cliente1[0]['EMAIL'];
        $_SESSION['reservaRentaCliente1'] = $cliente1[0]['EM_RENTALIQUIDA'];
        $_SESSION['reservaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
        $_SESSION['reservaInstitucionCliente1'] = $cliente1[0]['INSTITUCION'];
        $_SESSION['reservaRegionCliente1'] = $cliente1[0]['REGION'];
        $_SESSION['reservaActividadCliente1'] = $cliente1[0]['ACTIVIDAD'];
        $_SESSION['reservaPaisCliente1'] = $cliente1[0]['PAIS'];
        $_SESSION['reservaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
        $_SESSION['reservaResidenciaCliente1'] = $cliente1[0]['RESIDENCIA'];
        $_SESSION['reservaSexoCliente1'] = $cliente1[0]['SEXO'];
        $_SESSION['reservaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
        $_SESSION['reservaNivelEducCliente1'] = $cliente1[0]['NIVELEDUCACIONAL'];
        $_SESSION['reservaCasaHabitaCliente1'] = $cliente1[0]['CASAHABITA'];
        $_SESSION['reservaMotivoCompraCliente1'] = $cliente1[0]['MOTIVOCOMPRA'];

        $proyecto = consultaProyectoEspecifico($_SESSION['idProyecto']);

        $_SESSION['reservaInmobiliaria'] = $proyecto[0]['INMOBILIARIA'];
        $_SESSION['reservaInmobiliariaDireccion'] = $proyecto[0]['DIRECCION'];
        $_SESSION['reservaValidesReserva'] = $proyecto[0]['DIASRES'];

        $_SESSION['estacionamientosClienteReserva'] = $_POST['estacionamientosClienteReserva'];
        $_SESSION['bodegasClienteReserva'] = $_POST['bodegasClienteReserva'];

        //Actualiza datos de cliente en session_start
        $_SESSION['reservaNombreCliente'] = $cliente1[0]['NOMBRES'];
        $_SESSION['reservaApellidoCliente'] = $cliente1[0]['APELLIDOS'];

        //OBSERVACION
        $_SESSION['observacionCrearReserva'] = $observacionCrearReserva;

        echo "Ok¬".$_SESSION['reservaCodigoProyecto']."¬".$_SESSION['numeroOperacion'];
      }
      else{
        $row->query("ROLLBACK");
        echo "Sin datos";
      }
    }
  	else{
  		echo "Sin datos";
  	}
	}
	else{
		echo "Sin datos";
	}
?>
