<?php
ini_set('display_errors', 'On');
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');

	if(count($_POST) > 0){
    $codigoProyecto = $_POST['promesa_codigoProyecto'];
    $numeroOperacion = $_POST['promesa_numeroOperacion'];

    $consultaPromesa = consultaPromesaEspecifica($codigoProyecto,$numeroOperacion);

    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
    $idUnidad = $consultaPromesa[0]['IDUNIDAD'];
    //Cambio de estado a Unidad de la Promesa
    cambioEstadoUnidadesPromesa($idUnidad);

    liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion);
    liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);
    eliminaPromesaBodega($codigoProyecto, $numeroOperacion);
    eliminaPromesaEstacionamiento($codigoProyecto, $numeroOperacion);
    eliminaPromesaCuotas($codigoProyecto,$numeroOperacion);
    eliminarFormaPagoComisionSinCon($codigoProyecto, $numeroOperacion);
    eliminaComisionPromesa($codigoProyecto, $numeroOperacion);
    eliminaComentarioPromesa($codigoProyecto, $idUnidad, $numeroOperacion);
    eliminaGarantia($idPromesa);

    //Borrar documentos
    // $document = '/var/www/html/Git/inmonet';
    // $document = 'D:/MAMP/htdocs/inmonet';
    $document = '/home/livingne/inmonet.cl';

    $row = eliminaPromesa($codigoProyecto,$numeroOperacion);

		if($row == "Ok")
		{
		    $rutaPR = $document . '/repositorio/' . $consultaPromesa[0]['PR_PDF'];
		    if (file_exists($rutaPR))
		    {
		    unlink($rutaPR);
		    }

		    $rutaCP = $document . '/repositorio/' . $consultaPromesa[0]['CP_PDF'];
		    if (file_exists($rutaCP))
		    {
		    unlink($rutaCP);
		    }

		    $rutaPF = $document . '/repositorio/' . $consultaPromesa[0]['PF_PDF'];
		    if (file_exists($rutaPF))
		    {
		    unlink($rutaPF);
		    }

		    $rutaPS = $document . '/repositorio/' . $consultaPromesa[0]['PS_PDF'];
		    if (file_exists($rutaPS))
		    {
		    unlink($rutaPS);
		    }

		    if($consultaPromesa[0]['ESTADO'] != '1' && $consultaPromesa[0]['ESTADO'] != '7'){
		      //Cambia estado de reserva a reservada
		      cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 3);
		    }
		    echo "Ok";
		}
		else{
		  echo "Sin datos";
		}
	}
	else{
		echo "Sin datos por post";
	}
?>
