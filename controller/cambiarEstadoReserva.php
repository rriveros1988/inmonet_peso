<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');

	if(count($_POST) > 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$numeroOperacion = $_POST['numeroOperacion'];
			$estadoReserva = $_POST['estadoReserva'];

    	$row = cambiarEstadoReserva($codigoProyecto, $numeroOperacion, $estadoReserva);

    	if($row == "Ok")
    	{
				if($estadoReserva == '7'){
					liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
			    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
			    liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
				}
				else if($estadoReserva == '9'){
					liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
			    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
			    liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
				}
				echo "Ok";
			}
			else{
				echo "Sin datos";
			}
		}
		else{
    		echo "Sin datos";
  	}
?>
