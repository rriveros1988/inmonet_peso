google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawChart);
var h = $(window).height() - 270;
var w = $(window).width()/2;

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['DIA', 'Cantidad', { 'role': 'style' }, { 'role': 'annotation' },],
      ['Lunes', 0, '#58D3F7', '0'],
      ['Martes', 0, '#58D3F7', '0'],
      ['Miercoles', 0, '#58D3F7', '0'],
      ['Jueves', 0, '#58D3F7', '0'],
      ['Viernes', 0, '#58D3F7', '0'],
      ['Sabado', 0, '#58D3F7', '0'],
      ['Domingo', 0, '#58D3F7', '0']      
    ]);


    var options = {
        title: 'CONSULTA GARANTIAS POR DIA',
        legend: {position: 'none'},
        isStacked: false,
        titleTextStyle: {
    		color: '#FFF'
		},
        'chartArea': {'left': 35},
        annotations: {
            textStyle: {
                fontSize: 11,
                bold: true,
                color: '#FFF'
            }
        },
        vAxis: {
            textStyle: {
                fontSize: 12,
                bold: true,
                color: '#FFF'
            },
            viewWindow: {
                min: 0
            }
        },
        hAxis: {
            textStyle: {
                fontSize: 12,
                color: '#FFF'
            },
            slantedText: true, 
            slantedTextAngle: 50
        },
        'height':   h,
        'width':   w,
        animation:{
            duration: 1000,
            easing: 'out',
        },
        backgroundColor: { fill:'transparent' }
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('cantidadPorDia'));
    chart.draw(data, options);

    setTimeout(function(){ 
    	$.ajax({
		    url:   'controller/datosCantidadDia.php',
		    type:  'post',
		    success:  function (response) {
		        var p = response.split(",");
	            if(p != null){
	            	if(p[0] != "Sin datos" && p[0] != "Error"){
	            		for(var i = 0; i < p.length; i=i+2){
		                    var a = i + 1;
		                    switch (p[i]) {
		                        case 'Lunes':
		                        	data.setValue(0,1, p[a]);
	                                data.setValue(0,3, '' + p[a] + '');
		                        break;
		                        case 'Martes':
		                        	data.setValue(1,1, p[a]);
	                                data.setValue(1,3, '' + p[a] + '');
		                        break;
		                        case 'Miercoles':
		                        	data.setValue(2,1, p[a]);
	                                data.setValue(2,3, '' + p[a] + '');
		                        break;
		                        case 'Jueves':
		                        	data.setValue(3,1, p[a]);
	                                data.setValue(3,3, '' + p[a] + '');
		                        break;
		                        case 'Viernes':
		                        	data.setValue(4,1, p[a]);
	                                data.setValue(4,3, '' + p[a] + '');
		                        break;
		                        case 'Sabado':
		                        	data.setValue(5,1, p[a]);
	                                data.setValue(5,3, '' + p[a] + '');
		                        break;
		                        case 'Domingo':
		                        	data.setValue(6,1, p[a]);
	                                data.setValue(6,3, '' + p[a] + '');
		                        break;
		                   	}
		              	}
		              	
		              	//Re-Gráficar
	   		 			chart.draw(data, options);
	            	}
	            }
		  	}
		});
    },2500);                      
}